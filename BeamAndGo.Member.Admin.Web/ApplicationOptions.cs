﻿using System;

namespace BeamAndGo.Member.Admin.Web
{
    /// <summary>
    /// Sample file for application options to be loaded using 
    /// </summary>
    public class ApplicationOptions
    {
        /// <summary>
        /// CloudFront CDN URL for static assets.
        /// </summary>
        public string StaticAssetsUrl { get; set; } = "https://xxx.cloudfront.net";

        /// <summary>
        /// Member Web URL for redirect.
        /// </summary>
        public string MemberWebUrl { get; set; } = "https://member.beamandgo.com";
        
        /// <summary>
        /// Member mail verification url for mail
        /// </summary>
        public string MailVerificationUrl { get; set; } = "/verify";
        
        /// <summary>
        /// Error notifications get sent to this email address.
        /// </summary>
        public string ErrorNotificationEmail { get; set; } = "alert@domain.com";

        /// <summary>
        /// Default timezone used for date/time display
        /// </summary>
        public string TimeZone { get; set; } = "Asia/Manila";

        /// <summary>
        /// Secret used to validate JWT access tokens from OAuth2 server
        /// </summary>
        public string TokenSecret { get; set; } = "xxx";

        /// <summary>
        /// Auth Callback URL
        /// </summary>
        public string AuthCallbackUrl { get; set; } = "https://mydomain.com/auth/callback";

        /// <summary>
        /// Auth Redirect URL
        /// </summary>
        public string AuthRedirectUrl { get; set; } = "https://mydomain.com";

        /// <summary>
        /// Auth Signing Key for validating ID tokens
        /// </summary>
        public string AuthSigningKey { get; set; } = "xxx";

        /// <summary>
        /// Auth Scope to request
        /// </summary>
        public string AuthScope { get; set; } = "profile";

        /// <summary>
        /// Clock skew offset (in seconds) so we will attempt to refresh
        /// Access Tokens earlier than its actual expiry time
        /// </summary>
        public int AuthClockSkew { get; set; } = 30;

        /// <summary>
        /// Number of records to show on Summary screen
        /// </summary>
        public int SummaryCount { get; set; } = 5;

        public bool KYCEnabled { get; set; } = false;

        public int PageSize { get; set; } = 100;
    }
}
