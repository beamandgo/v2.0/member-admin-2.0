﻿using System.Linq;
using System.Collections.Generic;
using Newtonsoft.Json;
using JWT.Builder;
using JWT.Algorithms;

namespace BeamAndGo.Member.Admin.Web.Auth
{
    public class AccessToken
    {
        [JsonProperty("iss")]
        public string Issuer { get; set; }

        [JsonProperty("sub")]
        public string Subject { get; set; }

        [JsonProperty("aud")]
        public string Audience { get; set; }

        [JsonProperty("exp")]
        public long Expires { get; set; }

        [JsonProperty("iat")]
        public long IssuedAt { get; set; }

        [JsonProperty("nbf")]
        public long NotBefore { get; set; }

        [JsonProperty("jti")]
        public string JwtId { get; set; }

        [JsonProperty("client_id")]
        public string ClientId { get; set; }

        [JsonProperty("scope")]
        public string Scope { get; set; }

        [JsonIgnore]
        public string[] Scopes { get { return Scope?.Split(' '); } }

        public int ContainScopes(string scopes)
        {
            return ContainScopes(scopes?.Split(' '));
        }

        public int ContainScopes(string[] scopes)
        {
            return Scopes.Intersect(scopes).Count();
        }

        public static AccessToken CreateFrom(string jwt, string key = null, bool validate = false)
        {
            return new JwtBuilder()
                .WithAlgorithm(new HMACSHA256Algorithm())
                .WithSecret(key)
                .MustVerifySignature()
                .Decode<AccessToken>(jwt);
        }
    }
}