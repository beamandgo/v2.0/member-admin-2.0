﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using BeamAndGo.Auth.SDK;
using BeamAndGo.Member.Admin.Web.Models;
using JWT;
using JWT.Serializers;
using JWT.Algorithms;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using BeamAndGo.Member;
using Microsoft.Extensions.Logging;

namespace BeamAndGo.Member.Admin.Web.Controllers
{
    public class AuthController : Controller
    {
        private AuthClient _authClient;
        private ApplicationOptions _applicationOptions;
        private readonly ILogger _logger;

        public AuthController(ILogger<AuthController> logger, IOptions<ApplicationOptions> applicationOptions,  AuthClient authClient)
        {
            _applicationOptions = applicationOptions.Value;
            _authClient = authClient;
            _logger = logger;
        }

        [HttpGet]
        [Route("/login")]
        public IActionResult Login()
        {
            // Dynamically generate state and store in session
            var state = Guid.NewGuid().ToString();
            HttpContext.Session.SetString(SessionKey.Token.State, state);

            var loginUri = _authClient.GenerateLoginUri(_applicationOptions.AuthCallbackUrl, _applicationOptions.AuthScope, state);

            // Redirect to login
            return Redirect(loginUri.ToString());
        }

        [HttpGet]
        [Route("/signup")]
        public IActionResult Signup()
        {
            // Dynamically generate state and store in session
            var state = Guid.NewGuid().ToString();
            HttpContext.Session.SetString(SessionKey.Token.State, state);

            var signup = _authClient.GenerateLoginUri(AuthPromptType.Signup, _applicationOptions.AuthCallbackUrl, _applicationOptions.AuthScope, state).ToString();

            // Redirect to signup
            return Redirect(signup.ToString());
        }

        [HttpGet]
        [Route("/logout")]
        public IActionResult Logout()
        {
            HttpContext.Session.Clear();

            var logoutUri = _authClient.GenerateLogoutUri(_applicationOptions.AuthRedirectUrl);
            return Redirect(logoutUri.ToString());
        }

        [HttpGet]
        [Route("/auth/callback")]
        public async Task<IActionResult> Callback([FromQuery(Name = "code")] string code, [FromQuery(Name = "state")] string state)
        {
            try
            {
                var isMatchState = HttpContext.Session.GetString(SessionKey.Token.State)?.Equals(state) ?? false;

                if (!isMatchState)
                {
                    return RedirectToPage("/Error", new { error = ErrorMessage.InvalidState.Error, error_description = ErrorMessage.InvalidState.ErrorDescription });
                }

                var token = await _authClient.GetTokenFromAuthorizationCode(code);

                if (token == null)
                {
                    return RedirectToPage("/Error", new { error = ErrorMessage.InvalidCredentials.Error, error_description = ErrorMessage.InvalidCredentials.ErrorDescription });
                }

                var idToken = token.DecodeIdToken(_applicationOptions.AuthSigningKey);

                HttpContext.Session.SetString(SessionKey.Token.AccessToken, token.AccessToken);
                HttpContext.Session.SetString(SessionKey.Token.RefreshToken, token.RefreshToken);
                HttpContext.Session.SetString(SessionKey.Token.IdToken, token.IdToken);
                HttpContext.Session.SetString(SessionKey.Token.Scope, token.Scope);

                // We need to calculate when the token will expire, this will enable us to know when to ask for a refresh token
                HttpContext.Session.SetString(SessionKey.Token.Expiry, DateTimeOffset.UtcNow.AddSeconds(token.ExpiresIn).ToUnixTimeSeconds().ToString());
                // Store Member Sid
                HttpContext.Session.SetString(SessionKey.MemberSid, idToken.ContainsKey("sub") ? (string)idToken["sub"] : string.Empty);

                string redirectUrl = HttpContext.Session.GetString(SessionKey.RedirectUrl);
                if (!string.IsNullOrEmpty(redirectUrl))
                {
                    return new RedirectResult(redirectUrl);
                }

                return new RedirectResult("/index");
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);

                // Dynamically generate state and store in session
                var newState = Guid.NewGuid().ToString();
                HttpContext.Session.SetString(SessionKey.Token.State, newState);

                // Generate a login URI
                var loginUri = _authClient.GenerateLoginUri(AuthPromptType.Login, _applicationOptions.AuthCallbackUrl, _applicationOptions.AuthScope, newState);

                // Redirect to login
                return Redirect(loginUri.ToString());
            }
        }

    }
}