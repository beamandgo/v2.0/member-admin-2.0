﻿using BeamAndGo.Member;
using BeamAndGo.Member.Admin.Web.Auth;
using BeamAndGo.Member.Admin.Web.Filters;
using BeamAndGo.Member.Admin.Web.Models;
using BeamAndGo.Member.Admin.Web.Models.Request;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BeamAndGo.Member.Admin.Web.Controllers
{
    public partial class MemberController
    {
        [AccessTokenRequired("member member:admin", AccessTokenScopeCondition.Any)]
        [HttpGet]
        [Route("/api/member/{memberSid}/email")]
        public async Task<IActionResult> ListEmails(string memberSid)
        {
            try
            {
                var token = (AccessToken)HttpContext.Items["AccessToken"];
                var userSid = token.Subject;

                if (token.Scope.Contains("admin") || userSid == memberSid)
                {
                    List<MemberEmail> memberEmails = new List<MemberEmail>();

                    await foreach (var email in _memberEmailService.ListByMemberSid(memberSid))
                    {
                        memberEmails.Add(email);
                    }

                    return Ok(new MemberEmailListModel(memberEmails));
                }

                return BadRequest(ApiErrorMessage.ScopeNotGranted);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(new ApiErrorMessage { Code = ApiErrorMessage.InvalidRequest.Code, Message = ex.Message });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);

                return StatusCode(StatusCodes.Status500InternalServerError, ApiErrorMessage.ServerError);
            }
        }

        [AccessTokenRequired("member member:admin", AccessTokenScopeCondition.Any)]
        [HttpPost]
        [Route("/api/member/{memberSid}/email")]
        public async Task<IActionResult> CreateEmail(string memberSid, [FromBody] CreateEmailRequest request)        
        {
            try
            {
                var token = (AccessToken)HttpContext.Items["AccessToken"];
                var userSid = token.Subject;

                if (token.Scope.Contains("admin") || userSid == memberSid)
                {
                    var memberEmail = await _memberEmailService.Create(memberSid, request.Email);

                    return Ok(new MemberEmailModel(memberEmail));
                }

                return BadRequest(ApiErrorMessage.ScopeNotGranted);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(new ApiErrorMessage { Code = ApiErrorMessage.InvalidRequest.Code, Message = ex.Message });
            }
            catch (InvalidOperationException ex)
            {
                return BadRequest(new ApiErrorMessage { Code = ApiErrorMessage.InvalidOperation.Code, Message = ex.Message });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.ToString());

                return StatusCode(StatusCodes.Status500InternalServerError, ApiErrorMessage.ServerError);
            }
        }

        [AccessTokenRequired("member member:admin", AccessTokenScopeCondition.Any)]
        [HttpGet]
        [Route("/api/member/{memberSid}/email/{emailSid}")]
        public async Task<IActionResult> GetEmail(string memberSid, string emailSid)
        {
            try
            {
                var token = (AccessToken)HttpContext.Items["AccessToken"];
                var userSid = token.Subject;

                if (token.Scope.Contains("admin") || userSid == memberSid)
                {
                    var member = await _memberService.GetBySid(memberSid);
                    var email = await _memberEmailService.GetBySid(emailSid);

                    if (member == null || email == null)
                    {
                        return NotFound(ApiErrorMessage.NotFound);
                    }

                    if (email.MemberId != member.Id && !token.Scope.Contains("admin"))
                    {
                        return BadRequest(new ApiErrorMessage { Code = ApiErrorMessage.InvalidRequest.Code, Message = "Access denied" });
                    }

                    return Ok(new MemberEmailModel(email));
                }

                return BadRequest(ApiErrorMessage.ScopeNotGranted);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(new ApiErrorMessage { Code = ApiErrorMessage.InvalidRequest.Code, Message = ex.Message });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.ToString());

                return StatusCode(StatusCodes.Status500InternalServerError, ApiErrorMessage.ServerError);
            }
        }

        [AccessTokenRequired("member member:admin", AccessTokenScopeCondition.Any)]
        [HttpPut]
        [Route("/api/member/{memberSid}/email/{emailSid}")]
        public async Task<IActionResult> UpdateEmail(string memberSid, string emailSid, [FromBody] UpdateEmailRequest request)
        {
            try
            {
                var token = (AccessToken)HttpContext.Items["AccessToken"];
                var userSid = token.Subject;

                if (token.Scope.Contains("admin") || userSid == memberSid)
                {

                    var member = await _memberService.GetBySid(memberSid);
                    var email = await _memberEmailService.GetBySid(emailSid);

                    if (member == null || email == null)
                    {
                        return NotFound(ApiErrorMessage.NotFound);
                    }

                    if (email.MemberId != member.Id && !token.Scope.Contains("admin"))
                    {
                        return BadRequest(new ApiErrorMessage { Code = ApiErrorMessage.InvalidRequest.Code, Message = "Access denied" });
                    }

                    var memberEmail = await _memberEmailService.Update(emailSid, request.Email);

                    return Ok(new MemberEmailModel(memberEmail));
                }

                return BadRequest(ApiErrorMessage.ScopeNotGranted);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(new ApiErrorMessage { Code = ApiErrorMessage.InvalidRequest.Code, Message = ex.Message });
            }
            catch (InvalidOperationException ex)
            {
                return BadRequest(new ApiErrorMessage { Code = ApiErrorMessage.InvalidOperation.Code, Message = ex.Message });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.ToString());

                return StatusCode(StatusCodes.Status500InternalServerError, ApiErrorMessage.ServerError);
            }
        }

        [HttpPost]
        [Route("/api/email/{emailSid}/verify")]
        public async Task<IActionResult> VerifyEmail(string emailSid, VerifyEmailRequest request)
        {
            try
            {   
                var verificationResult = await _memberEmailVerificationService.Verify(request.VerificationCode);
                var email = await _memberEmailService.GetBySid(emailSid);

                return Ok(new MemberEmailModel(email));
            }
            catch (ArgumentException ex)
            {
                return BadRequest(new ApiErrorMessage { Code = ApiErrorMessage.InvalidRequest.Code, Message = ex.Message });
            }
            catch (InvalidOperationException ex)
            {
                return BadRequest(new ApiErrorMessage { Code = ApiErrorMessage.InvalidOperation.Code, Message = ex.Message });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.ToString());

                return StatusCode(StatusCodes.Status500InternalServerError, ApiErrorMessage.ServerError);
            }
        }

        [AccessTokenRequired("member member:admin", AccessTokenScopeCondition.Any)]
        [HttpPost]
        [Route("/api/member/{memberSid}/email/{emailSid}/default")]
        public async Task<IActionResult> SetDefaultEmail(string memberSid, string emailSid)
        {
            try
            {
                var token = (AccessToken)HttpContext.Items["AccessToken"];
                var userSid = token.Subject;

                if (token.Scope.Contains("admin") || userSid == memberSid)
                {
                    var member = await _memberService.GetBySid(memberSid);
                    var email = await _memberEmailService.GetBySid(emailSid);

                    if (member == null || email == null)
                    {
                        return NotFound(ApiErrorMessage.NotFound);
                    }

                    if (email.MemberId != member.Id && !token.Scope.Contains("admin"))
                    {
                        return BadRequest(new ApiErrorMessage { Code = ApiErrorMessage.InvalidRequest.Code, Message = "Access denied" });
                    }
                    
                    await _memberEmailService.SetDefault(emailSid);

                    var emailResult = await _memberEmailService.GetBySid(emailSid);

                    return Ok(new MemberEmailModel(emailResult));
                }

                return BadRequest(ApiErrorMessage.ScopeNotGranted);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(new ApiErrorMessage { Code = ApiErrorMessage.InvalidRequest.Code, Message = ex.Message });
            }
            catch (InvalidOperationException ex)
            {
                return BadRequest(new ApiErrorMessage { Code = ApiErrorMessage.InvalidOperation.Code, Message = ex.Message });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.ToString());

                return StatusCode(StatusCodes.Status500InternalServerError, ApiErrorMessage.ServerError);
            }
        }

        [AccessTokenRequired("member member:admin", AccessTokenScopeCondition.Any)]
        [HttpDelete]
        [Route("/api/member/{memberSid}/email/{emailSid}")]
        public async Task<IActionResult> DeleteEmail(string memberSid, string emailSid)
        {
            try
            {
                var token = (AccessToken)HttpContext.Items["AccessToken"];
                var userSid = token.Subject;

                if (token.Scope.Contains("admin") || userSid == memberSid)
                {
                    var member = await _memberService.GetBySid(memberSid);
                    var email = await _memberEmailService.GetBySid(emailSid);

                    if (member == null || email == null)
                    {
                        return NotFound(ApiErrorMessage.NotFound);
                    }

                    if (email.MemberId != member.Id && !token.Scope.Contains("admin"))
                    {
                        return BadRequest(new ApiErrorMessage { Code = ApiErrorMessage.InvalidRequest.Code, Message = "Access denied" });
                    }

                    await _memberEmailService.Delete(emailSid);

                    return Ok(new MemberEmailModel(email));
                }

                return BadRequest(ApiErrorMessage.ScopeNotGranted);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(new ApiErrorMessage { Code = ApiErrorMessage.InvalidRequest.Code, Message = ex.Message });
            }
            catch (InvalidOperationException ex)
            {
                return BadRequest(new ApiErrorMessage { Code = ApiErrorMessage.InvalidOperation.Code, Message = ex.Message });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.ToString());

                return StatusCode(StatusCodes.Status500InternalServerError, ApiErrorMessage.ServerError);
            }

        }
    }
}
