﻿using BeamAndGo.Member;
using BeamAndGo.Member.Admin.Web.Auth;
using BeamAndGo.Member.Admin.Web.Filters;
using BeamAndGo.Member.Admin.Web.Models;
using BeamAndGo.Member.Admin.Web.Models.Request;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeamAndGo.Member.Admin.Web.Controllers
{
    public partial class MemberController
    {
        [AccessTokenRequired("member member:admin", AccessTokenScopeCondition.Any)]
        [HttpGet]
        [Route("/api/member/{memberSid}/location")]
        public async Task<IActionResult> ListLocations(string memberSid)
        {
            try
            {
                var token = (AccessToken)HttpContext.Items["AccessToken"];
                var userSid = token.Subject;

                if (token.Scope.Contains("admin") || userSid == memberSid)
                {
                    List<MemberAddress> memberAddresses = new List<MemberAddress>();

                    await foreach (var address in _memberAddressService.ListByMemberSid(memberSid))
                    {
                        memberAddresses.Add(address);
                    }

                    return Ok(new MemberAddressListModel(memberAddresses));
                }

                return BadRequest(ApiErrorMessage.ScopeNotGranted);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(new ApiErrorMessage { Code = ApiErrorMessage.InvalidRequest.Code, Message = ex.Message });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);

                return StatusCode(StatusCodes.Status500InternalServerError, ApiErrorMessage.ServerError);
            }
        }

        [AccessTokenRequired("member member:admin", AccessTokenScopeCondition.Any)]
        [HttpPost]
        [Route("/api/member/{memberSid}/location")]
        public async Task<IActionResult> CreateLocation(string memberSid, [FromBody] CreateAddressRequest request)
        {
            try
            {
                var token = (AccessToken)HttpContext.Items["AccessToken"];
                var userSid = token.Subject;

                if (token.Scope.Contains("admin") || userSid == memberSid)
                {
                    var address = await _memberAddressService.Create(memberSid, request.Name, request.Street, request.PostalCode, request.CountryCode);

                    return Ok(new MemberAddressModel(address));
                }

                return BadRequest(ApiErrorMessage.ScopeNotGranted);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(new ApiErrorMessage { Code = ApiErrorMessage.InvalidRequest.Code, Message = ex.Message });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);

                return StatusCode(StatusCodes.Status500InternalServerError, ApiErrorMessage.ServerError);
            }
        }

        [AccessTokenRequired("member member:admin", AccessTokenScopeCondition.Any)]
        [HttpGet]
        [Route("/api/member/{memberSid}/location/{locationSid}")]
        public async Task<IActionResult> GetLocation(string memberSid, string locationSid)
        {
            try
            {
                var token = (AccessToken)HttpContext.Items["AccessToken"];
                var userSid = token.Subject;

                if (token.Scope.Contains("admin") || userSid == memberSid)
                {
                    var member = await _memberService.GetBySid(memberSid);
                    var address = await _memberAddressService.GetBySid(locationSid);

                    if (member == null || address == null)
                    {
                        return NotFound(ApiErrorMessage.NotFound);
                    }

                    if (address.MemberId != member.Id && !token.Scope.Contains("admin"))
                    {
                        return BadRequest(new ApiErrorMessage { Code = ApiErrorMessage.InvalidRequest.Code, Message = "Access denied" });
                    }

                    return Ok(new MemberAddressModel(address));
                }

                return BadRequest(ApiErrorMessage.ScopeNotGranted);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(new ApiErrorMessage { Code = ApiErrorMessage.InvalidRequest.Code, Message = ex.Message });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);

                return StatusCode(StatusCodes.Status500InternalServerError, ApiErrorMessage.ServerError);
            }
        }

        [AccessTokenRequired("member member:admin", AccessTokenScopeCondition.Any)]
        [HttpPut]
        [Route("/api/member/{memberSid}/location/{locationSid}")]
        public async Task<IActionResult> UpdateLocation(string memberSid, string locationSid, [FromBody] UpdateAddressRequest request)
        {
            try
            {
                var token = (AccessToken)HttpContext.Items["AccessToken"];
                var userSid = token.Subject;

                if (token.Scope.Contains("admin") || userSid == memberSid)
                {
                    var member = await _memberService.GetBySid(memberSid);
                    var address = await _memberAddressService.GetBySid(locationSid);

                    if (member == null || address == null)
                    {
                        return NotFound(ApiErrorMessage.NotFound);
                    }

                    if (address.MemberId != member.Id && !token.Scope.Contains("admin"))
                    {
                        return BadRequest(new ApiErrorMessage { Code = ApiErrorMessage.InvalidRequest.Code, Message = "Access denied" });
                    }
                    
                    address = await _memberAddressService.Update(locationSid, request.Name, request.Street, request.PostalCode, request.CountryCode);

                    return Ok(new MemberAddressModel(address));
                }

                return BadRequest(ApiErrorMessage.ScopeNotGranted);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(new ApiErrorMessage { Code = ApiErrorMessage.InvalidRequest.Code, Message = ex.Message });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);

                return StatusCode(StatusCodes.Status500InternalServerError, ApiErrorMessage.ServerError);
            }
        }

        [AccessTokenRequired("member member:admin", AccessTokenScopeCondition.Any)]
        [HttpDelete]
        [Route("/api/member/{memberSid}/location/{locationSid}")]
        public async Task<IActionResult> DeleteLocation(string memberSid, string locationSid)
        {
            try
            {
                var token = (AccessToken)HttpContext.Items["AccessToken"];
                var userSid = token.Subject;

                if (token.Scope.Contains("admin") || userSid == memberSid)
                {
                    var member = await _memberService.GetBySid(memberSid);
                    var address = await _memberAddressService.GetBySid(locationSid);

                    if (member == null || address == null)
                    {
                        return NotFound(ApiErrorMessage.NotFound);
                    }

                    if (address.MemberId != member.Id && !token.Scope.Contains("admin"))
                    {
                        return BadRequest(new ApiErrorMessage { Code = ApiErrorMessage.InvalidRequest.Code, Message = "Access denied" });
                    }
                    
                    await _memberAddressService.Delete(locationSid);

                    return Ok(new MemberAddressModel(address));
                }

                return BadRequest(ApiErrorMessage.ScopeNotGranted);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(new ApiErrorMessage { Code = ApiErrorMessage.InvalidRequest.Code, Message = ex.Message });
            }
            catch (InvalidOperationException ex)
            {
                return BadRequest(new ApiErrorMessage { Code = ApiErrorMessage.InvalidOperation.Code, Message = ex.Message });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);

                return StatusCode(StatusCodes.Status500InternalServerError, ApiErrorMessage.ServerError);
            }
        }
    }
}
