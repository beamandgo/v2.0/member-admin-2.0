﻿using BeamAndGo.Member;
using BeamAndGo.Member.Admin.Web.Auth;
using BeamAndGo.Member.Admin.Web.Filters;
using BeamAndGo.Member.Admin.Web.Models;
using BeamAndGo.Member.Admin.Web.Models.Request;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeamAndGo.Member.Admin.Web.Controllers
{
    public partial class MemberController
    {
        [AccessTokenRequired("member member:admin", AccessTokenScopeCondition.Any)]
        [HttpGet]
        [Route("/api/member/{memberSid}/phone")]
        public async Task<IActionResult> ListPhones(string memberSid)
        {
            try
            {
                var token = (AccessToken)HttpContext.Items["AccessToken"];
                var userSid = token.Subject;

                if (token.Scope.Contains("admin") || userSid == memberSid)
                {
                    List<MemberPhone> memberPhones = new List<MemberPhone>();

                    await foreach (var phone in _memberPhoneService.ListByMemberSid(memberSid))
                    {
                        memberPhones.Add(phone);
                    }

                    return Ok(new MemberPhoneListModel(memberPhones));
                }

                return BadRequest(ApiErrorMessage.ScopeNotGranted);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(new ApiErrorMessage { Code = ApiErrorMessage.InvalidRequest.Code, Message = ex.Message });
            }
            catch (MemberServiceException ex)
            {
                return BadRequest(new ApiErrorMessage { Code = ApiErrorMessage.MemberServiceError.Code, Message = ex.Message });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);

                return StatusCode(StatusCodes.Status500InternalServerError, ApiErrorMessage.ServerError);
            }
        }

        [AccessTokenRequired("member member:admin", AccessTokenScopeCondition.Any)]
        [HttpPost]
        [Route("/api/member/{memberSid}/phone")]
        public async Task<IActionResult> CreatePhone(string memberSid, [FromBody] CreatePhoneRequest request)
        {
            try
            {
                var token = (AccessToken)HttpContext.Items["AccessToken"];
                var userSid = token.Subject;

                if (token.Scope.Contains("admin") || userSid == memberSid)
                {
                    var phone = await _memberPhoneService.Create(memberSid, request.CallingCode, request.PhoneNumber);

                    return Ok(new MemberPhoneModel(phone));
                }

                return BadRequest(ApiErrorMessage.ScopeNotGranted);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(new ApiErrorMessage { Code = ApiErrorMessage.InvalidRequest.Code, Message = ex.Message });
            }
            catch (MemberServiceException ex)
            {
                return BadRequest(new ApiErrorMessage { Code = ApiErrorMessage.MemberServiceError.Code, Message = ex.Message });
            }
            catch (InvalidOperationException ex)
            {
                return BadRequest(new ApiErrorMessage { Code = ApiErrorMessage.InvalidOperation.Code, Message = ex.Message });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.ToString());

                return StatusCode(StatusCodes.Status500InternalServerError, ApiErrorMessage.ServerError);
            }
        }

        [AccessTokenRequired("member member:admin", AccessTokenScopeCondition.Any)]
        [HttpGet]
        [Route("/api/member/{memberSid}/phone/{phoneSid}")]
        public async Task<IActionResult> GetPhone(string memberSid, string phoneSid)
        {
            try
            {
                var token = (AccessToken)HttpContext.Items["AccessToken"];
                var userSid = token.Subject;

                if (token.Scope.Contains("admin") || userSid == memberSid)
                {
                    var member = await _memberService.GetBySid(memberSid);
                    var phone = await _memberPhoneService.GetBySid(phoneSid);

                    if (member == null || phone == null)
                    {
                        return NotFound(ApiErrorMessage.NotFound);
                    }

                    if (phone.MemberId != member.Id && !token.Scope.Contains("admin"))
                    {
                        return BadRequest(new ApiErrorMessage { Code = ApiErrorMessage.InvalidRequest.Code, Message = "Access denied" });
                    }

                    phone = await _memberPhoneService.GetBySid(phoneSid);

                    return Ok(new MemberPhoneModel(phone));
                }

                return BadRequest(ApiErrorMessage.ScopeNotGranted);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(new ApiErrorMessage { Code = ApiErrorMessage.InvalidRequest.Code, Message = ex.Message });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.ToString());

                return StatusCode(StatusCodes.Status500InternalServerError, ApiErrorMessage.ServerError);
            }
        }

        [AccessTokenRequired("member member:admin", AccessTokenScopeCondition.Any)]
        [HttpPut]
        [Route("/api/member/{memberSid}/phone/{phoneSid}")]
        public async Task<IActionResult> UpdatePhone(string memberSid, string phoneSid, [FromBody] UpdatePhoneRequest request)
        {
            try
            {
                var token = (AccessToken)HttpContext.Items["AccessToken"];
                var userSid = token.Subject;

                if (token.Scope.Contains("admin") || userSid == memberSid)
                {
                    var member = await _memberService.GetBySid(memberSid);
                    var phone = await _memberPhoneService.GetBySid(phoneSid);

                    if (member == null || phone == null)
                    {
                        return NotFound(ApiErrorMessage.NotFound);
                    }

                    if (phone.MemberId != member.Id && !token.Scope.Contains("admin"))
                    {
                        return BadRequest(new ApiErrorMessage { Code = ApiErrorMessage.InvalidRequest.Code, Message = "Access denied" });
                    }

                    phone = await _memberPhoneService.Update(phoneSid, request.CallingCode, request.PhoneNumber);

                    return Ok(new MemberPhoneModel(phone));
                }

                return BadRequest(ApiErrorMessage.ScopeNotGranted);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(new ApiErrorMessage { Code = ApiErrorMessage.InvalidRequest.Code, Message = ex.Message });
            }
            catch (InvalidOperationException ex)
            {
                return BadRequest(new ApiErrorMessage { Code = ApiErrorMessage.InvalidOperation.Code, Message = ex.Message });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.ToString());

                return StatusCode(StatusCodes.Status500InternalServerError, ApiErrorMessage.ServerError);
            }
        }

        [HttpPost]
        [Route("/api/phone/{phoneSid}/verify")]
        public async Task<IActionResult> VerifyPhone(string phoneSid, VerifyPhoneRequest request)
        {
            try
            {
                var verificationResult = await _memberPhoneVerificationService.Verify(phoneSid, string.Empty, request.VerificationCode);
                var phone = await _memberPhoneService.GetBySid(phoneSid);

                return Ok(new MemberPhoneModel(phone));
            }
            catch (ArgumentException ex)
            {
                return BadRequest(new ApiErrorMessage { Code = ApiErrorMessage.InvalidRequest.Code, Message = ex.Message });
            }
            catch (InvalidOperationException ex)
            {
                return BadRequest(new ApiErrorMessage { Code = ApiErrorMessage.InvalidOperation.Code, Message = ex.Message });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.ToString());

                return StatusCode(StatusCodes.Status500InternalServerError, ApiErrorMessage.ServerError);
            }
        }

        [AccessTokenRequired("member member:admin", AccessTokenScopeCondition.Any)]
        [HttpPost]
        [Route("/api/member/{memberSid}/phone/{phoneSid}/default")]
        public async Task<IActionResult> SetDefaultPhone(string memberSid, string phoneSid)
        {
            try
            {
                var token = (AccessToken)HttpContext.Items["AccessToken"];
                var userSid = token.Subject;

                if (token.Scope.Contains("admin") || userSid == memberSid)
                {
                    var member = await _memberService.GetBySid(memberSid);
                    var phone = await _memberPhoneService.GetBySid(phoneSid);

                    if (member == null || phone == null)
                    {
                        return NotFound(ApiErrorMessage.NotFound);
                    }

                    if (phone.MemberId != member.Id && !token.Scope.Contains("admin"))
                    {
                        return BadRequest(new ApiErrorMessage { Code = ApiErrorMessage.InvalidRequest.Code, Message = "Access denied" });
                    }

                    await _memberPhoneService.SetDefault(phoneSid);

                    var phoneResult = await _memberPhoneService.GetBySid(phoneSid);

                    return Ok(new MemberPhoneModel(phoneResult));
                }

                return BadRequest(ApiErrorMessage.ScopeNotGranted);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(new ApiErrorMessage { Code = ApiErrorMessage.InvalidRequest.Code, Message = ex.Message });
            }
            catch (InvalidOperationException ex)
            {
                return BadRequest(new ApiErrorMessage { Code = ApiErrorMessage.InvalidOperation.Code, Message = ex.Message });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.ToString());

                return StatusCode(StatusCodes.Status500InternalServerError, ApiErrorMessage.ServerError);
            }
        }

        [AccessTokenRequired("member member:admin", AccessTokenScopeCondition.Any)]
        [HttpDelete]
        [Route("/api/member/{memberSid}/phone/{phoneSid}")]
        public async Task<IActionResult> DeletePhone(string memberSid, string phoneSid)
        {
            try
            {
                var token = (AccessToken)HttpContext.Items["AccessToken"];
                var userSid = token.Subject;

                if (token.Scope.Contains("admin") || userSid == memberSid)
                {
                    var member = await _memberService.GetBySid(memberSid);
                    var phone = await _memberPhoneService.GetBySid(phoneSid);

                    if (member == null || phone == null)
                    {
                        return NotFound(ApiErrorMessage.NotFound);
                    }

                    if (phone.MemberId != member.Id && !token.Scope.Contains("admin"))
                    {
                        return BadRequest(new ApiErrorMessage { Code = ApiErrorMessage.InvalidRequest.Code, Message = "Access denied" });
                    }

                    await _memberPhoneService.Delete(phoneSid);

                    return Ok(new MemberPhoneModel(phone));
                }

                return BadRequest(ApiErrorMessage.ScopeNotGranted);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(new ApiErrorMessage { Code = ApiErrorMessage.InvalidRequest.Code, Message = ex.Message });
            }
            catch (InvalidOperationException ex)
            {
                return BadRequest(new ApiErrorMessage { Code = ApiErrorMessage.InvalidOperation.Code, Message = ex.Message });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.ToString());

                return StatusCode(StatusCodes.Status500InternalServerError, ApiErrorMessage.ServerError);
            }

        }
    }
}
