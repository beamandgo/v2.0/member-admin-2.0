﻿using BeamAndGo.Member;
using BeamAndGo.Member.Admin.Web.Auth;
using BeamAndGo.Member.Admin.Web.Filters;
using BeamAndGo.Member.Admin.Web.Models;
using BeamAndGo.Member.Admin.Web.Models.Request;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeamAndGo.Member.Admin.Web.Controllers
{
    public partial class MemberController
    {
        [AccessTokenRequired("member member:admin", AccessTokenScopeCondition.Any)]
        [HttpGet]
        [Route("/api/member/{memberSid}/relationship")]
        public async Task<IActionResult> ListRelationships(string memberSid)
        {
            try
            {
                var token = (AccessToken)HttpContext.Items["AccessToken"];
                var userSid = token.Subject;

                if (token.Scope.Contains("admin") || userSid == memberSid)
                {
                    List<MemberRelationship> memberRelationship = new List<MemberRelationship>();

                    await foreach (var relationship in _memberRelationshipService.ListByMemberSid(memberSid))
                    {
                        memberRelationship.Add(relationship);
                    }

                    return Ok(new MemberRelationshipListModel(memberRelationship));
                }

                return BadRequest(ApiErrorMessage.ScopeNotGranted);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(new ApiErrorMessage { Code = ApiErrorMessage.InvalidRequest.Code, Message = ex.Message });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);

                return StatusCode(StatusCodes.Status500InternalServerError, ApiErrorMessage.ServerError);
            }
        }

        [AccessTokenRequired("member member:admin", AccessTokenScopeCondition.Any)]
        [HttpPost]
        [Route("/api/member/{memberSid}/relationship")]
        public async Task<IActionResult> CreateRelationship(string memberSid, [FromBody] CreateMemberRelationshipData request)
        {
            try
            {
                var token = (AccessToken)HttpContext.Items["AccessToken"];
                var userSid = token.Subject;

                if (token.Scope.Contains("admin") || userSid == memberSid)
                {
                    var relationship = await _memberRelationshipService.Create(request); 
                    return Ok(new MemberRelationshipModel(relationship));
                }

                return BadRequest(ApiErrorMessage.ScopeNotGranted);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(new ApiErrorMessage { Code = ApiErrorMessage.InvalidRequest.Code, Message = ex.Message });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.ToString());

                return StatusCode(StatusCodes.Status500InternalServerError, ApiErrorMessage.ServerError);
            }
        }

        [AccessTokenRequired("member member:admin", AccessTokenScopeCondition.Any)]
        [HttpPost]
        [Route("/api/member/{memberSid}/relationship/{relationshipSid}")]
        public async Task<IActionResult> UpdateRelationship(string memberSid, string relationshipSid, [FromBody] UpdateMemberRelationshipData request)
        { 
            try
            {
                var token = (AccessToken)HttpContext.Items["AccessToken"];
                var userSid = token.Subject;

                if (token.Scope.Contains("admin") || userSid == memberSid)
                {
                    var member = await _memberService.GetBySid(memberSid);
                    var relationship = await _memberRelationshipService.GetBySid(relationshipSid);

                    if (member == null || relationship == null)
                    {
                        return NotFound(ApiErrorMessage.NotFound);
                    }

                    if (relationship.MemberId != member.Id && !token.Scope.Contains("admin"))
                    {
                        return BadRequest(new ApiErrorMessage { Code = ApiErrorMessage.InvalidRequest.Code, Message = "Access denied" });
                    }
                    
                    relationship = await _memberRelationshipService.Update(memberSid, request);

                    return Ok(new MemberRelationshipModel(relationship));
                }

                return BadRequest(ApiErrorMessage.ScopeNotGranted);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(new ApiErrorMessage { Code = ApiErrorMessage.InvalidRequest.Code, Message = ex.Message });
            }
            catch (MemberServiceException ex)
            {
                return BadRequest(new ApiErrorMessage { Code = ApiErrorMessage.MemberServiceError.Code, Message = ex.Message });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.ToString());

                return StatusCode(StatusCodes.Status500InternalServerError, ApiErrorMessage.ServerError);
            }
        }

        [AccessTokenRequired("member member:admin", AccessTokenScopeCondition.Any)]
        [HttpDelete]
        [Route("/api/member/{memberSid}/relationship/{relationshipSid}")]
        public async Task<IActionResult> DeleteRelationship(string memberSid, string relationshipSid)
        {
            try
            {
                var token = (AccessToken)HttpContext.Items["AccessToken"];
                var userSid = token.Subject;

                if (token.Scope.Contains("admin") || userSid == memberSid)
                {
                    var member = await _memberService.GetBySid(memberSid);
                    var relationship = await _memberRelationshipService.GetBySid(relationshipSid);

                    if (member == null || relationship == null)
                    {
                        return NotFound(ApiErrorMessage.NotFound);
                    }

                    if (relationship.MemberId != member.Id && !token.Scope.Contains("admin"))
                    {
                        return BadRequest(new ApiErrorMessage { Code = ApiErrorMessage.InvalidRequest.Code, Message = "Access denied" });
                    }

                    await _memberRelationshipService.Delete(relationshipSid);

                    return Ok(new MemberRelationshipModel(relationship));
                }

                return BadRequest(ApiErrorMessage.ScopeNotGranted);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(new ApiErrorMessage { Code = ApiErrorMessage.InvalidRequest.Code, Message = ex.Message });
            }
            catch (MemberServiceException ex)
            {
                return BadRequest(new ApiErrorMessage { Code = ApiErrorMessage.MemberServiceError.Code, Message = ex.Message });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.ToString());

                return StatusCode(StatusCodes.Status500InternalServerError, ApiErrorMessage.ServerError);
            }
        }
    }
}
