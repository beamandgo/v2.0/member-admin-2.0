﻿using BeamAndGo.Member;
using BeamAndGo.Member.Verification;
using BeamAndGo.Member.Admin.Web.Auth;
using BeamAndGo.Member.Admin.Web.Filters;
using BeamAndGo.Member.Admin.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MemberListModel = BeamAndGo.Member.Admin.Web.Models.MemberListModel;

namespace BeamAndGo.Member.Admin.Web.Controllers
{
    public partial class MemberController : Controller
    {
        private readonly IMemberService _memberService;
        private readonly IMemberAddressService _memberAddressService;
        private readonly IMemberEmailService _memberEmailService;
        private readonly IMemberEmailVerificationService _memberEmailVerificationService;
        private readonly IMemberLogService _memberLogService;
        private readonly IMemberPhoneService _memberPhoneService;
        private readonly IMemberPhoneVerificationService _memberPhoneVerificationService;
        private readonly IMemberRelationshipService _memberRelationshipService;
        private readonly ILogger _logger;

        public MemberController(IMemberService memberService, 
                                IMemberAddressService memberAddressService, 
                                IMemberEmailService memberEmailService,
                                IMemberEmailVerificationService memberEmailVerificationService,
                                IMemberLogService memberLogService, 
                                IMemberPhoneService memberPhoneService,
                                IMemberPhoneVerificationService memberPhoneVerificationService,
                                IMemberRelationshipService memberRelationshipService,  
                                ILogger<MemberController> logger)
        {
            _memberService = memberService;
            _memberAddressService = memberAddressService;
            _memberEmailService = memberEmailService;
            _memberEmailVerificationService = memberEmailVerificationService;
            _memberLogService = memberLogService;
            _memberPhoneService = memberPhoneService;
            _memberPhoneVerificationService = memberPhoneVerificationService;
            _memberRelationshipService = memberRelationshipService;
            _logger = logger;
        }
        
        [AccessTokenRequired("member:admin", AccessTokenScopeCondition.All)]
        [HttpGet]
        [Route("/api/member")]
        public async Task<IActionResult> List()
        {
            try
            {
                var members = new List<Member>(await _memberService.ListAll());

                if (members.Count == 0)
                {
                    return NotFound(ApiErrorMessage.NoMembersFound);
                }

                return Ok(new MemberListModel(members));

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);

                return StatusCode(StatusCodes.Status500InternalServerError, ApiErrorMessage.ServerError);
            }
        }

        [AccessTokenRequired("member:admin", AccessTokenScopeCondition.All)]
         [HttpPost]
        [Route("/api/member")]
        public async Task<IActionResult> CreateMember([FromBody] CreateMemberData request)
        {
            try
            {
                var member = await _memberService.Create(request);

                return Ok(new MemberModel(member));
            }
            catch (ArgumentException ex)
            {
                return BadRequest(new ApiErrorMessage { Code = ApiErrorMessage.InvalidRequest.Code, Message = ex.Message });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.ToString());

                return StatusCode(StatusCodes.Status500InternalServerError, ApiErrorMessage.ServerError);
            }
        }

        [AccessTokenRequired("member member:admin", AccessTokenScopeCondition.Any)]
        [HttpGet]
        [Route("/api/member/{memberSid}")]
        public async Task<IActionResult> Get(string memberSid)
        {
            try
            {
                var token = (AccessToken)HttpContext.Items["AccessToken"];
                var userSid = token.Subject;

                if(token.Scope.Contains("admin") || userSid == memberSid)
                {
                    var member = await _memberService.GetBySid(memberSid);

                    return Ok(new MemberModel(member));
                }

                return BadRequest(ApiErrorMessage.ScopeNotGranted);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(new ApiErrorMessage { Code = ApiErrorMessage.InvalidRequest.Code, Message = ex.Message });
            }
            catch (MemberServiceException ex)
            {
                return BadRequest(new ApiErrorMessage { Code = ApiErrorMessage.MemberServiceError.Code, Message = ex.Message });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);

                return StatusCode(StatusCodes.Status500InternalServerError, ApiErrorMessage.ServerError);
            }
        }

        [AccessTokenRequired("member member:admin", AccessTokenScopeCondition.Any)]
        [HttpPut]
        [Route("/api/member/{memberSid}")]
        public async Task<IActionResult> Put(string memberSid, [FromBody] UpdateMemberData request)
        {
            try
            {
                var token = (AccessToken)HttpContext.Items["AccessToken"];
                var userSid = token.Subject;

                if (token.Scope.Contains("admin") || userSid == memberSid)
                {
                    var member = await _memberService.Update(memberSid, request);

                    return Ok(new MemberModel(member));
                }

                return BadRequest(ApiErrorMessage.ScopeNotGranted);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(new ApiErrorMessage { Code = ApiErrorMessage.InvalidRequest.Code, Message = ex.Message });
            }
            catch (MemberServiceException ex)
            {
                return BadRequest(new ApiErrorMessage { Code = ApiErrorMessage.MemberServiceError.Code, Message = ex.Message });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);

                return StatusCode(StatusCodes.Status500InternalServerError, ApiErrorMessage.ServerError);
            }
        }
        
        [AccessTokenRequired("member:admin", AccessTokenScopeCondition.All)]
        [HttpDelete]
        [Route("/api/member/{memberSid}")]
        public async Task<IActionResult> Delete(string memberSid)
        {
            try
            {
                var member = await _memberService.Disable(memberSid);

                return Ok(new MemberModel(member));
            }
            catch (ArgumentException ex)
            {
                return BadRequest(new ApiErrorMessage { Code = ApiErrorMessage.InvalidRequest.Code, Message = ex.Message });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);

                return StatusCode(StatusCodes.Status500InternalServerError, ApiErrorMessage.ServerError);
            }
        }
        
        #region Log
        [AccessTokenRequired("member:admin", AccessTokenScopeCondition.All)]
        [HttpGet]
        [Route("/api/member/{memberId}/log")]
        public async Task<IActionResult> GetLog(int memberId)
        {
            try
            {
                List<MemberLog> memberLogs = null;

                await foreach (var log in _memberLogService.ListByMemberId(memberId))
                {
                    memberLogs.Add(log);
                }    

                return Ok(new MemberLogListModel(memberLogs));
            }
            catch (ArgumentException ex)
            {
                return BadRequest(new ApiErrorMessage { Code = ApiErrorMessage.InvalidRequest.Code, Message = ex.Message });
            }
            catch (MemberServiceException ex)
            {
                return BadRequest(new ApiErrorMessage { Code = ApiErrorMessage.MemberServiceError.Code, Message = ex.Message });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);

                return StatusCode(StatusCodes.Status500InternalServerError, ApiErrorMessage.ServerError);
            }
        }
        #endregion
    }
}
