﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using BeamAndGo.Member.Admin.Web.Auth;
using BeamAndGo.Member.Admin.Web.Models;
using System.Linq;

namespace BeamAndGo.Member.Admin.Web.Filters
{
    public class AccessTokenRequiredAttribute : Attribute, IAsyncResourceFilter
    {
        public bool IsReusable => false;

        private string[] _requestedScopes;
        private AccessTokenScopeCondition _requestedCondition;

        public AccessTokenRequiredAttribute(string requestedScopes, AccessTokenScopeCondition requestedCondition)
        {
            _requestedScopes = requestedScopes.Split(' ');
            _requestedCondition = requestedCondition;
        }

        public async Task OnResourceExecutionAsync(ResourceExecutingContext context, ResourceExecutionDelegate next)
        {
            var options = context.HttpContext.RequestServices.GetService<IOptions<ApplicationOptions>>().Value;
            var logger = context.HttpContext.RequestServices.GetService<ILogger<AccessTokenRequiredAttribute>>();

            if (!context.HttpContext.Request.Headers.ContainsKey("Authorization"))
            {
                logger.LogDebug("Authorization required.");
                context.Result = new UnauthorizedObjectResult(ApiErrorMessage.AuthorizationRequired);

                return;
            }
            
            var authorization = context.HttpContext.Request.Headers["Authorization"].ToString().Split(' ');

            if (authorization.Length != 2 || authorization[0] != "Bearer")
            {
                logger.LogWarning("Bearer token not found or malformed.");
                context.Result = new UnauthorizedObjectResult(ApiErrorMessage.TokenRequired);
                return;
            }

            try
            {
                var token = AccessToken.CreateFrom(authorization[1], options.TokenSecret, true);

                if (token == null || token.Scope == null)
                {
                    logger.LogWarning("No scope found in token.");
                    context.Result = new UnauthorizedObjectResult(ApiErrorMessage.ScopeRequired);
                    return;
                }

                if ((_requestedCondition == AccessTokenScopeCondition.All && token.ContainScopes(_requestedScopes) != _requestedScopes.Count()) ||
                    (_requestedCondition == AccessTokenScopeCondition.Any && token.ContainScopes(_requestedScopes) < 1))
                {
                    logger.LogWarning("Requested scopes not granted.");
                    context.Result = new UnauthorizedObjectResult(ApiErrorMessage.ScopeNotGranted);
                    return;
                }

                logger.LogInformation("Authorization successful.");

                context.HttpContext.Items.Add("AccessToken", token);

                await next();
            }
            catch (JWT.Exceptions.InvalidTokenPartsException ex)
            {
                logger.LogWarning(ex, "Invalid toke parts.");
                context.Result = new UnauthorizedObjectResult(ApiErrorMessage.InvalidToken);
                return;
            }
            catch (JWT.Exceptions.TokenExpiredException ex)
            {
                logger.LogWarning(ex, "Token expired.");
                context.Result = new UnauthorizedObjectResult(ApiErrorMessage.ExpiredToken);
                return;
            }
            catch (JWT.Exceptions.SignatureVerificationException ex)
            {
                logger.LogWarning(ex, "Token signature verication failed.");
                context.Result = new UnauthorizedObjectResult(ApiErrorMessage.SignatureTokenVerificationFailed);
                return;
            }
            catch (Exception ex)
            {
                logger.LogWarning(ex, "Error decoding JWT.");
                context.Result = new UnauthorizedObjectResult(ApiErrorMessage.ServerError);
                return;
            }
        }
    }
}
