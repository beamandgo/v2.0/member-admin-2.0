﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Mvc.Filters;

namespace BeamAndGo.Member.Admin.Web.Filters
{
    public class LayoutPageFilter : IAsyncPageFilter
    {
        private readonly ApplicationOptions _applicationOptions;

        public LayoutPageFilter(IOptions<ApplicationOptions> applicationOptions)
        {
            _applicationOptions = applicationOptions.Value;
        }

        public async Task OnPageHandlerSelectionAsync(PageHandlerSelectedContext context)
        {
            await Task.CompletedTask;
        }

        public async Task OnPageHandlerExecutionAsync(PageHandlerExecutingContext context, PageHandlerExecutionDelegate next)
        {
            context.HttpContext.Items["StaticAssetsUrl"] = _applicationOptions.StaticAssetsUrl;
            await next();
        }
    }
}
