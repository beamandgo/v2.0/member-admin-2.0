﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Http.Extensions;
using BeamAndGo.Member;
using BeamAndGo.Member.Admin.Web.Models;
using BeamAndGo.Core.Data;
using BeamAndGo.Core.Common;
using BeamAndGo.Member.Admin;

namespace BeamAndGo.Member.Admin.Web.Filters
{
    public class LoginRequiredAttribute : Attribute, IAsyncResourceFilter
    {
        public LoginRequiredAttribute()
        {
           
        }

        public async Task OnResourceExecutionAsync(ResourceExecutingContext context, ResourceExecutionDelegate next)
        {
            try
            {
                if (!context.HttpContext.Session.IsAvailable || string.IsNullOrEmpty(context.HttpContext.Session.GetString(SessionKey.MemberSid)))
                {
                    var currentUrl = UriHelper.GetDisplayUrl(context.HttpContext.Request);

                    context.HttpContext.Session.SetString(SessionKey.RedirectUrl, currentUrl);

                    context.HttpContext.Response.Redirect("/login");
                    return;
                }

                var memberService = context.HttpContext.RequestServices.GetService<IMemberService>();
                var MemberAdminService = context.HttpContext.RequestServices.GetService<IMemberAdminService>();
               

                var memberSid = context.HttpContext.Session.GetString(SessionKey.MemberSid);
                var member = await memberService.GetBySid(memberSid);
                var admin = await MemberAdminService.GetByMemberSid(memberSid);

                if (admin.RecordStatus == RecordStatus.Inactive)
                {
                    context.HttpContext.Response.Redirect($"/Error?error={ErrorMessage.AccessDenied.Error}&error_description={ErrorMessage.AccessDenied.ErrorDescription}");
                    return;
                }

                context.HttpContext.Items.Add(ItemKey.Member.Profile, member);
                context.HttpContext.Items.Add(ItemKey.Member.Admin, admin);
            }
            catch (Exception ex)
            {
                if (ExceptionExtensions.GetCode(ex) == "MEMBERADMIN_ADMIN_NOTFOUND" || ExceptionExtensions.GetCode(ex) == "MEMBER_NOTFOUND")
                {
                    context.HttpContext.Response.Redirect($"/Error?error={ErrorMessage.AccessDenied.Error}&error_description={ErrorMessage.AccessDenied.ErrorDescription}");
                    return;
                }
                else
                {
                    throw;
                }
            }

            await next();
        }
    }
}
