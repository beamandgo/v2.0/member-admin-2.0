﻿using System;
namespace BeamAndGo.Member.Admin.Web
{
    public static class ItemKey
    {
        public const string Accounts = "Accounts";

        public class Member
        {
            public const string Admin = "Member.Admin";
            public const string Profile = "Member.Profile";
            public const string Country = "Member.Country";
            public const string memberSid = "Member.Sid";
        }
    }
}
