﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeamAndGo.Member.Admin.Web.Models
{
    public class ApiErrorMessage
    {
        public static ApiErrorMessage AuthorizationRequired = new ApiErrorMessage { Code = "authorization_required", Message = "Authorization required" };
        public static ApiErrorMessage TokenRequired = new ApiErrorMessage { Code = "token_required", Message = "Bearer token not found or malformed" };
        public static ApiErrorMessage ScopeRequired = new ApiErrorMessage { Code = "scope_required", Message = "No scope found in token" };
        public static ApiErrorMessage ScopeNotGranted = new ApiErrorMessage { Code = "scope_not_granted", Message = "Requested scopes not granted" };

        public static ApiErrorMessage ServerError = new ApiErrorMessage { Code = "server_error", Message = "Server error." };
        public static ApiErrorMessage ExpiredToken = new ApiErrorMessage { Code = "expired_token", Message = "Token already expired." };

        public static ApiErrorMessage InvalidRequest = new ApiErrorMessage { Code = "invalid_request", Message = "Invalid request." };
        public static ApiErrorMessage InvalidOperation = new ApiErrorMessage { Code = "invalid_operation", Message = "Invalid Opreration." };
        public static ApiErrorMessage MemberServiceError = new ApiErrorMessage { Code = "member_service_error", Message = "Member Service Error." };

        public static ApiErrorMessage InvalidMember = new ApiErrorMessage { Code = "invalid_member", Message = "Invalid member." };
        public static ApiErrorMessage NoMembersFound = new ApiErrorMessage { Code = "no_members_found", Message = "No members found." };
        public static ApiErrorMessage FirstNameRequired = new ApiErrorMessage { Code = "first_name_required", Message = "Firstname required." };
        public static ApiErrorMessage LastNameRequired = new ApiErrorMessage { Code = "last_name_required", Message = "Lastname required." };
        public static ApiErrorMessage InvalidToken = new ApiErrorMessage { Code = "invalid_token", Message = "Invalid or malformed token." };
        public static ApiErrorMessage SignatureTokenVerificationFailed = new ApiErrorMessage { Code = "signature_token_verification_failed", Message = "Token signature verification failed." };
        public static ApiErrorMessage CountryCodeRequired = new ApiErrorMessage { Code = "country_code_required", Message = "Country code requied." };

        public static ApiErrorMessage NotFound = new ApiErrorMessage { Code = "record_not_found", Message = "Record not found." };

        public string Code { get; set; }

        public string Message { get; set; }
    }
}
