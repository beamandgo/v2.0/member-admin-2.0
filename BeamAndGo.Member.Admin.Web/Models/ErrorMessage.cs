﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace BeamAndGo.Member.Admin.Web.Models
{
    public class ErrorMessage
    {
        public static ErrorMessage InvalidState = new ErrorMessage { Error = "invalid_state", ErrorDescription = "Invalid State" };
        public static ErrorMessage InvalidCredentials = new ErrorMessage { Error = "invalid_credentials", ErrorDescription = "Invalid Credentials" };

        public static ErrorMessage InvalidRequest = new ErrorMessage { Error = "invalid_request", ErrorDescription = "Invalid request" };
        public static ErrorMessage UnauthorizedClient = new ErrorMessage { Error = "unauthorized_client", ErrorDescription = "Unauthorized client" };
        public static ErrorMessage AccessDenied = new ErrorMessage { Error = "access_denied", ErrorDescription = "Access denied" };
        public static ErrorMessage UnsupportedResponseType = new ErrorMessage { Error = "unsupported_response_type", ErrorDescription = "Unsupported response type" };
        public static ErrorMessage InvalidScope = new ErrorMessage { Error = "invalid_scope", ErrorDescription = "Invalid scope" };
        public static ErrorMessage ServerError = new ErrorMessage { Error = "server_error", ErrorDescription = "Server error" };
        public static ErrorMessage TemporarilyUnavailable = new ErrorMessage { Error = "temporarily_unavailable", ErrorDescription = "Temporarily unavailable" };

        public static ErrorMessage ErrorMessageDefault = new ErrorMessage { ErrorDescription = "Sorry, we are currently experiencing technical difficulties. Please try again or contact customer service for further assistance" };
        public static ErrorMessage ErrorMessagePostfix = new ErrorMessage { ErrorDescription = "Please try again or contact customer service for further assistance" };

        [JsonPropertyName("error")]
        public string Error { get; set; }

        [JsonPropertyName("error_description")]
        public string ErrorDescription { get; set; }

        [JsonPropertyName("error_uri")]
        public string ErrorUri { get; set; }
    }
}
