﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BeamAndGo.Member.Admin.Web.Models.FormData
{
    public class AddressFormData
    {
        [Display(Name = "Name")]
        [Required(ErrorMessage = "Enter name")]
        public string Name { get; set; }

        [Display(Name = "Street")]
        [Required(ErrorMessage = "Enter Street")]
        public string Street { get; set; }

        [Display(Name = "Postal Code")]
        [Required(ErrorMessage = "Enter Postal Code")]
        public string PostalCode { get; set; }

        [Display(Name = "Country")]
        [Required(ErrorMessage = "Select Country")]
        public string Country { get; set; }
    }
}
