﻿using BeamAndGo.Member;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BeamAndGo.Member.Admin.Web.Models.FormData
{
    public class CreateMemberFormData
    {
        [Display(Name = "First Name")]
        [Required(ErrorMessage = "Enter first name")]
        public string FirstName { get; set; }

        [Display(Name = "Middle Name")]
        public string MiddleName { get; set; }

        [Display(Name = "Last Name")]
        [Required(ErrorMessage = "Enter last name")]
        public string LastName { get; set; }

        [Display(Name = "Display Name/Nickname")]
        [Required(ErrorMessage = "Enter nickname")]
        public string DisplayName { get; set; }

        [Display(Name = "Birthdate")]
        [Required(ErrorMessage = "Enter birthdate")]
        public string Birthdate { get; set; }

        [Display(Name = "Gender")]
        [Required(ErrorMessage = "Select gender")]
        public char? Gender;

        [Display(Name = "Country Code")]
        [Required(ErrorMessage = "Invalid calling code")]
        public string CallingCode { get; set; }

        [Display(Name = "Mobile Number")]
        [Required(ErrorMessage = "Please enter a numeric mobile number.")]
        public string MobileNumber { get; set; }

        [Display(Name = "Email")]
        [DataType(DataType.EmailAddress)]
        [Required(ErrorMessage = "Please enter a valid email address.")]
        public string Email { get; set; }

        [Display(Name = "Name")]
        [Required(ErrorMessage = "Please enter valid name.")]
        public string Name { get; set; }

        [Display(Name = "Address")]
        [Required(ErrorMessage = "Please enter an address.")]
        public string Address { get; set; }

        [Display(Name = "Postal Code")]
        [Required(ErrorMessage = "Please enter a postal code.")]
        public string PostalCode { get; set; }

        [Display(Name = "Country")]
        [Required(ErrorMessage = "Invalid country code.")]
        public string CountryCode { get; set; }

        [Display(Name = "Relationship")]
        [Required(ErrorMessage = "Please select relationship.")]
        public string RelationshipType { get; set; }
       
    }
}
