﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BeamAndGo.Member.Admin.Web.Models.FormData
{
    public class PhoneFormData
    {
        public string CountryCode { get; set; }
        public string CallingCode { get; set; }

        [Display(Name = "MobilePhone")]
        [Required(ErrorMessage = "Enter mobile phone number")]
        public string MobileNumber { get; set; }
    }
}
