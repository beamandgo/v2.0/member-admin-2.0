﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeamAndGo.Member.Admin.Web.Models.FormData
{
    public class PrivacySettingsFormData
    {
        public bool IsProfileChecked { get; set; }
        public bool IsPhoneChecked { get; set; }
        public bool IsEmailChecked { get; set; }
        public bool IsAddressChecked { get; set; }
    }
}
