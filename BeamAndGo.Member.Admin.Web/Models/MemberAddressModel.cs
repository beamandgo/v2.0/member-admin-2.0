﻿using BeamAndGo.Core.Data;
using BeamAndGo.Member;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeamAndGo.Member.Admin.Web.Models
{
    public class MemberAddressModel
    {
        public int Id { get; set; }
        public string Sid { get; set; }
        public string Name { get; set; }
        public string Street { get; set; }
        public string PostalCode { get; set; }
        public string CountryCode { get; set; }
        public bool IsDefault { get; set; }
        public bool IsVerified { get; set; }
        public DateTimeOffset? VerifiedAt { get; set; }
        public string VerificationMeta { get; set; }
        public string Remarks { get; set; }

        public MemberAddressModel(MemberAddress memberAddress)
        {
            Id = memberAddress.Id;
            Sid = memberAddress.Sid;
            Name = memberAddress.Name;
            Street = memberAddress.Street;
            PostalCode = memberAddress.PostalCode;
            CountryCode = memberAddress.CountryCode;
            IsDefault = memberAddress.IsDefault;
            IsVerified = memberAddress.IsVerified;
            VerifiedAt = memberAddress.VerifiedAt;
            VerificationMeta = memberAddress.VerificationMeta;
            Remarks = memberAddress.Remarks;
        }
    }
}
