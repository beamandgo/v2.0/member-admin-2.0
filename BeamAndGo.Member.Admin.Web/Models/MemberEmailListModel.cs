﻿using BeamAndGo.Member;
using System.Collections.Generic;

namespace BeamAndGo.Member.Admin.Web.Models
{
    public class MemberEmailListModel
    {
        public IList<MemberEmailModel> MemberEmails { get; set; } = new List<MemberEmailModel>();

        public MemberEmailListModel()
        {

        }

        public MemberEmailListModel(IList<MemberEmail> emails)
        {
            foreach (var email in emails)
                MemberEmails.Add(new MemberEmailModel(email));
        }
    }
}
