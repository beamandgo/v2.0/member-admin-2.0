﻿using System;
using BeamAndGo.Core.Data;
using BeamAndGo.Member;

namespace BeamAndGo.Member.Admin.Web.Models
{
    public class MemberEmailModel
    {
        public int Id { get; set; }
        public string Sid { get; set; }
        public string Identifier { get; set; }
        public string Email { get; set; }
        public bool IsDefault { get; set; }
        public bool IsVerified { get; set; }
        public DateTimeOffset? VerifiedAt { get; set; }
        public string VerificationMeta { get; set; }

        public MemberEmailModel() { }

        public MemberEmailModel(MemberEmail memberEmail)
        {
            Id = memberEmail.Id;
            Sid = memberEmail.Sid;
            Identifier = memberEmail.Identifier;
            Email = memberEmail.Email;
            IsDefault = memberEmail.IsDefault;
            IsVerified = memberEmail.IsVerified;
            VerifiedAt = memberEmail.VerifiedAt;
            VerificationMeta = memberEmail.VerificationMeta;
        }
    }


}
