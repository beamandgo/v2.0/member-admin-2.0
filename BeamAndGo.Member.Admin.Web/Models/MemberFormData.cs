﻿using BeamAndGo.Member;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BeamAndGo.Member.Admin.Web.Models
{
    public class MemberFormData
    {
        public string Sid { get; set; }

        [Display(Name = "First Name")]
        [Required(ErrorMessage = "Enter first name")]
        public string FirstName { get; set; }

        [Display(Name = "Middle Name")]
        public string MiddleName { get; set; }

        [Display(Name = "Last Name")]
        [Required(ErrorMessage = "Enter last name")]
        public string LastName { get; set; }

        [Display(Name = "Display Name/Nickname")]
        [Required(ErrorMessage = "Enter nickname")]
        public string DisplayName { get; set; }

        [Display(Name = "Birthdate")]
        [Required(ErrorMessage = "Enter birthdate")]
        public string Birthdate { get; set; }

        [Display(Name = "Gender")]
        [Required(ErrorMessage = "Select gender")]
        public char? Gender { get; set; }
    }
}
