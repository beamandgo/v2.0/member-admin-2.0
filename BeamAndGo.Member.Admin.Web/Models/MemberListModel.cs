﻿using System.Collections.Generic;

namespace BeamAndGo.Member.Admin.Web.Models
{
    public class MemberListModel
    {
        public IList<MemberModel> Members { get; set; } = new List<MemberModel>();

        public MemberListModel()
        {

        }

        public MemberListModel(IList<Member> members)
        {
            foreach (var member in members)
                Members.Add(new MemberModel(member));
        }
    }
}
