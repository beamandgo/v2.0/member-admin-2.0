﻿using BeamAndGo.Member;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeamAndGo.Member.Admin.Web.Models
{
    public class MemberLogListModel
    {
        public IList<MemberLogModel> MemberLogs { get; set; } = new List<MemberLogModel>();

        public MemberLogListModel()
        {

        }

        public MemberLogListModel(IList<MemberLog> logs)
        {
            foreach (var log in logs)
                MemberLogs.Add(new MemberLogModel(log));
        }
    }
}
