﻿using BeamAndGo.Member;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeamAndGo.Member.Admin.Web.Models
{
    public class MemberLogModel
    {
        public long Id { get; set; }
        public int MemberId { get; set; }
        public string Meta { get; set; }
        public string Notes { get; set; }
        public DateTimeOffset CreateDate { get; set; }
        public DateTimeOffset? UpdateDate { get; set; }

        public MemberLogModel(MemberLog memberLog)
        {
            Id = memberLog.Id;
            Meta = memberLog.Meta;
            Notes = memberLog.Notes;
            CreateDate = memberLog.CreateDate;
            UpdateDate = memberLog.UpdateDate;
        }
    }

}
