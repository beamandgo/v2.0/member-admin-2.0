﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BeamAndGo.Core.Data;

namespace BeamAndGo.Member.Admin.Web.Models
{
    public class MemberModel
    {
        public int Id { get; set; }
        public string Sid { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CountryCode { get; set; }
        public string BirthDate { get; set; }
        public RecordStatus Status { get; set; }

        public MemberModel() { }
        
        public MemberModel(Member member)
        {
            Id = member.Id;
            Sid = member.Sid;
            FirstName = member.FirstName;
            LastName = member.LastName;
            CountryCode = member.CountryCode;
            //BirthDate = new DateTime(member.BirthYear, member.BirthMonth, member.BirthDay).ToString("dd-MM-yyyy");
            Status = member.RecordStatus;
        }
    }
}
