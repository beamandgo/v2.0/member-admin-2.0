﻿using BeamAndGo.Member;
using System.Collections.Generic;

namespace BeamAndGo.Member.Admin.Web.Models
{
    public class MemberPhoneListModel
    {
        public IList<MemberPhoneModel> MemberPhones { get; set; } = new List<MemberPhoneModel>();

        public MemberPhoneListModel()
        {

        }

        public MemberPhoneListModel(IList<MemberPhone> phones)
        {
            foreach (var phone in phones)
                MemberPhones.Add(new MemberPhoneModel(phone));
        }
    }
}
