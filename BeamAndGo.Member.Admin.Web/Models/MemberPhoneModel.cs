﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BeamAndGo.Core.Data;
using BeamAndGo.Member;
using BNGM = BeamAndGo.Member;

namespace BeamAndGo.Member.Admin.Web.Models
{
    public class MemberPhoneModel
    {
        public int Id { get; set; }
        public string Sid { get; set; }
        public string Identifier { get; set; }
        public string CallingCode { get; set; }
        public string PhoneNumber { get; set; }
        public bool IsDefault { get; set; }
        public bool IsVerified { get; set; }
        public DateTimeOffset? VerifiedAt { get; set; }
        public string VerificationMeta { get; set; }

        public MemberPhoneModel() { }
        public MemberPhoneModel(MemberPhone memberPhone)
        {
            Id = memberPhone.Id;
            Sid = memberPhone.Sid;
            Identifier = memberPhone.Identifier;
            CallingCode = memberPhone.CallingCode;
            PhoneNumber = memberPhone.PhoneNumber;
            IsDefault = memberPhone.IsDefault;
            IsVerified = memberPhone.IsVerified;
            VerifiedAt = memberPhone.VerifiedAt;
            VerificationMeta = memberPhone.VerificationMeta;
        }
        
    }
    
}
