﻿using BeamAndGo.Member;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeamAndGo.Member.Admin.Web.Models
{
    public class MemberRelationshipListModel
    {
        public IList<MemberRelationshipModel> MemberRelationships { get; set; } = new List<MemberRelationshipModel>();

        public MemberRelationshipListModel()
        {

        }

        public MemberRelationshipListModel(IList<MemberRelationship> relationships)
        {
            foreach (var relationship in relationships)
                MemberRelationships.Add(new MemberRelationshipModel(relationship));
        }
    }
}
