﻿using BeamAndGo.Member;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeamAndGo.Member.Admin.Web.Models
{
    public class MemberRelationshipModel
    {
        public int Id { get; set; }
        public string Sid { get; set; }
        public int MemberId { get; set; }
        public int RelationshipId { get; set; }

        public MemberRelationshipModel(MemberRelationship memberRelationship)
        {
            Id = memberRelationship.Id;
            Sid = memberRelationship.Sid;
            MemberId = memberRelationship.MemberId;
            RelationshipId = memberRelationship.RelationshipTypeId;
        }
    }
}
