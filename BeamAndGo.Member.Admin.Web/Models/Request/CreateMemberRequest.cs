﻿using BeamAndGo.Member;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeamAndGo.Member.Admin.Web.Models.Request
{
    public class CreateMemberRequest
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CountryCode { get; set; }
        public string DisplayName { get; set; }
        public string MiddleName { get; set; }
        public string Birthdate { get; set; }
        public char? Gender { get; set; }
    }
}
