﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeamAndGo.Member.Admin.Web.Models.Request
{
    public class UpdatePhoneRequest
    {
        public string CallingCode { get; set; }
        public string PhoneNumber { get; set; }
    }
}
