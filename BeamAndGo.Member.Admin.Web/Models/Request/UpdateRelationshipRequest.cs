﻿using BeamAndGo.Member;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeamAndGo.Member.Admin.Web.Models.Request
{
    public class UpdateRelationshipRequest
    {
        public string MemberSid { get; set; }
        public string RelationshipTypeSid { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MobileCallingCode { get; set; }
        public string MobileNumber { get; set; }
        public string CountryCode { get; set; }
        public string Email { get; set; }
        public string Street { get; set; }
        public string PostalCode { get; set; }
        public char? Gender { get; set; }
        public string MiddleName { get; set; }
        public string BirthDate { get; set; }
    }
}
