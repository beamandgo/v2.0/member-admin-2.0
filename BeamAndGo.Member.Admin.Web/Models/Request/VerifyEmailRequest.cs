﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeamAndGo.Member.Admin.Web.Models.Request
{
    public class VerifyEmailRequest
    {
        public virtual string VerificationCode { get; set; }
    }
}
