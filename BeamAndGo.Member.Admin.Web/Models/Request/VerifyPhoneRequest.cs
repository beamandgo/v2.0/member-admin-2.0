﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeamAndGo.Member.Admin.Web.Models.Request
{
    public class VerifyPhoneRequest
    {
        public virtual string VerificationCode { get; set; }
    }
}
