﻿using System.Text.Json.Serialization;

namespace BeamAndGo.Member.Admin.Web.Models
{
    public class DataResponse<T> : StatusResponse
    {
        [JsonPropertyName("data")]
        public T Data { get; set; }
    }
}
