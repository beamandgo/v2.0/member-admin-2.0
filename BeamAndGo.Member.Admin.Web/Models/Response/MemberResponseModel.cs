﻿using Newtonsoft.Json;

namespace BeamAndGo.Member.Admin.Web.Models
{
    public class MemberResponseModel
    {
        [JsonProperty("success")]
        public bool Success { get; set; }

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("sid")]
        public string Sid { get; set; }

        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }

        [JsonProperty("countryCode")]
        public string CountryCode { get; set; }

        [JsonProperty("mobileCallingCode")]
        public string MobileCallingCode { get; set; }

        [JsonProperty("mobileNumber")]
        public string MobileNumber { get; set; }

        [JsonProperty("verifiedMobile")]
        public string VerifiedMobile { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("verifiedEmail")]
        public string VerifiedEmail { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }
    }
}
