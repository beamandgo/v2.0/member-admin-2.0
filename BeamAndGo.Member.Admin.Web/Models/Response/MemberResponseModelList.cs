﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeamAndGo.Member.Admin.Web.Models.Response
{
    public class MemberResponseModelList
    {
        [JsonProperty("members")]
        public IEnumerable<MemberResponseModel> Member { get; set; }
    }
}
