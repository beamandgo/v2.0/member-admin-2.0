﻿using System.Text.Json.Serialization;

namespace BeamAndGo.Member.Admin.Web.Models
{
    public class StatusResponse
    {
        [JsonPropertyName("success")]
        public bool Success { get; set; }

        [JsonPropertyName("statusCode")]
        public int StatusCode { get; set; }

        [JsonPropertyName("message")]
        public string Message { get; set; }
    }
}
