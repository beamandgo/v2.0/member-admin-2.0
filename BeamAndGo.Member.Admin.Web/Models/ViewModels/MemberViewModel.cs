﻿using BeamAndGo.Core.Data;
using BeamAndGo.Member;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BeamAndGo.Member.Admin.Web.ViewModels
{
    public class MemberViewModel
    {
        private readonly ILogger<MemberViewModel> _logger;

        public int Id { get; set; }
        public string Sid { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public char? Gender { get; set; }
        public string Address { get; set; }
        public string CountryCode { get; set; }
        public string MobileCallingCode { get; set; }
        public string MobileNumber { get; set; }
        public bool VerifiedMobile { get; set; }
        public string Email { get; set; }
        public bool VerifiedEmail { get; set; }
        public string BirthDate { get; set; }
        public RecordStatus Status { get; set; }

        public MemberPhone DefaultPhone;
        public MemberEmail DefaultEmail;
        public MemberAddress DefaultAddress;

        public MemberViewModel(ILogger<MemberViewModel> logger)
        {
            _logger = logger;
        }

        public MemberViewModel(Member member, bool isMasked)
        {
            try
            {
                Id = member.Id;
                Sid = member.Sid;
                FirstName = member.FirstName;
                MiddleName = member.MiddleName;
                LastName = member.LastName;
                CountryCode = member.CountryCode;

                Gender = member.Gender;
                Status = member.RecordStatus;

                if (member.Addresses != null)
                {
                    DefaultAddress = member.Addresses.Select(x => x).Where(x => x.MemberId == member.Id && x.IsDefault).FirstOrDefault();

                    if (DefaultAddress != null)
                    {
                        Address = $"{DefaultAddress.Street} {DefaultAddress.PostalCode} {DefaultAddress.CountryCode}".Trim();
                    }
                }

                if (member.Emails != null)
                {
                    DefaultEmail = member.Emails.Select(x => x).Where(x => x.MemberId == member.Id && x.IsDefault).FirstOrDefault();

                    if (DefaultEmail != null)
                    {
                        Email = string.IsNullOrEmpty(DefaultEmail.Email) ? "" : DefaultEmail.Email;
                        VerifiedEmail = DefaultEmail.IsVerified;
                    }
                }

                if (member.Phones != null)
                {
                    DefaultPhone = member.Phones.Select(x => x).Where(x => x.MemberId == member.Id && x.IsDefault).FirstOrDefault();

                    if (DefaultPhone != null)
                    {
                        MobileCallingCode = string.IsNullOrEmpty(DefaultPhone.CallingCode) ? "" : DefaultPhone.CallingCode;
                        MobileNumber = string.IsNullOrEmpty(DefaultPhone.PhoneNumber) ? "" : DefaultPhone.PhoneNumber;
                        VerifiedMobile = DefaultPhone.IsVerified;
                    }
                }

                if (member.BirthDay == null || member.BirthMonth == null || member.BirthYear == null)
                {
                    BirthDate = string.Empty;
                }
                else
                {
                    if ((int)member.BirthMonth > 12 || (int)member.BirthDay > 31 || (int)member.BirthDay <= 0)
                    {
                        BirthDate = string.Empty;
                    }
                    else
                    {
                        BirthDate = new DateTime((int)member.BirthYear, (int)member.BirthMonth, (int)member.BirthDay).ToString("yyyy/MM/dd");
                    }
                }

                if (isMasked)
                {
                    string emailMaskingPattern = @"(?<=[\w]{1})[\w-\._\+%]*(?=[\w]{1}@)";
                    string phoneMaskingPattern = @"(?<=[\d]{1})[\d]*(?=[\d]{1})";
                    string defaultMaskingPattern = @"(?!^)[/\S+/](?!$)";

                    MobileNumber = string.IsNullOrEmpty(MobileNumber) ? "" : Regex.Replace(MobileNumber, phoneMaskingPattern, m => new string('*', m.Length));
                    Email = string.IsNullOrEmpty(Email) ? "" : Regex.Replace(Email, emailMaskingPattern, m => new string('*', m.Length));
                    Address = string.IsNullOrEmpty(Address) ? "" : Regex.Replace(Address, defaultMaskingPattern, m => new string('*', m.Length));
                    BirthDate = string.IsNullOrEmpty(BirthDate) ? "" : Regex.Replace(BirthDate, defaultMaskingPattern, m => new string('*', m.Length));
                }

            }
            catch (Exception ex)
            {
                if (ex is ArgumentException || ex is InvalidOperationException)
                {
                    _logger.LogInformation(ex, ex.Message);
                }
                else
                {
                    _logger.LogError(ex, ex.Message);
                }
            }
        }
    }

}
