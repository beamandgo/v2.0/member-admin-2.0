﻿using BeamAndGo.Member;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeamAndGo.Member.Admin.Web.Models.ViewModels
{
    public class RelationshipGroupViewModel
    {
        public Member Member { get; set; }
        public MemberRelationship Relationship { get; set; }
    }
}
