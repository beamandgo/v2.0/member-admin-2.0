using System;
using System.Threading.Tasks;
using BeamAndGo.Core.Common;
using BeamAndGo.Member;
using BeamAndGo.Member.Admin;
using BeamAndGo.Member.Admin.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;

namespace BeamAndGo.Member.Admin.Web.Pages.Admin
{
    public class CreateAdminModel : PageModel
    {
        private readonly ILogger<CreateAdminModel> _logger; 
        private readonly IMemberAdminService _MemberAdminService;
        private readonly IMemberService _memberService;

        public bool IsXHR { get { return Request.Headers["X-Requested-With"] == "XMLHttpRequest"; } }
        public BeamAndGo.Member.Member Member { get; set; }
        public string MinExpiryDate { get; set; }


        [BindProperty]
        public DateTimeOffset? ExpiryDate { get; set; }
        [BindProperty]
        public MemberAdminAccessLevel AccessLevel { get; set; }
        [BindProperty]
        public string AdminMemberSid { get; set; }
        [BindProperty(SupportsGet = true)]
        public string SearchKeyword { get; set; }

        
        public CreateAdminModel(ILogger<CreateAdminModel> logger, 
                                IMemberAdminService MemberAdminService,
                                IMemberService memberService)
        {
            _logger = logger;
            _MemberAdminService = MemberAdminService;
            _memberService = memberService;
        }

        public IActionResult OnGet()
        {
            MinExpiryDate = DateTime.Now.ToString("yyyy-MM-dd");
            return Page();
        }

        public async Task<IActionResult> OnPostSearchAsync()
        {
            if (!string.IsNullOrEmpty(SearchKeyword))
            {
                try
                {
                    Member = await _memberService.SearchExact(SearchKeyword);

                    if (Member == null) return BadRequest("No member found");

                    return new OkObjectResult(Member);
                }
                catch (ArgumentException ex)
                {
                    _logger.LogInformation(ex, ex.Message);
                    return BadRequest($"{ExceptionExtensions.GetDescription(ex)}");
                }
                catch (Exception ex)
                {
                    if (ex is InvalidOperationException)
                    {
                        _logger.LogInformation(ex, ex.Message);
                        return BadRequest($"{ExceptionExtensions.GetDescription(ex)} {ErrorMessage.ErrorMessagePostfix.ErrorDescription}");
                    }
                    else
                    {
                        _logger.LogError(ex, ex.Message);
                        return BadRequest(ErrorMessage.ErrorMessageDefault.ErrorDescription);
                    }
                }
            }

            return BadRequest("Please enter keyword to search member.");
        }

        public async Task<IActionResult> OnPostAddAsync()
        {
            try
            {
                var MemberAdmin = await _MemberAdminService.Create(AdminMemberSid, AccessLevel, ExpiryDate);

                HttpContext.Session.SetString(SessionKey.NotificationMessage.Sucess, "Successfully added a new admin");
            }
            catch (ArgumentException ex)
            {
                _logger.LogInformation(ex, ex.Message);
                HttpContext.Session.SetString(SessionKey.NotificationMessage.Error, $"{ExceptionExtensions.GetDescription(ex)}");
            }
            catch (Exception ex)
            {
                if (ex is InvalidOperationException)
                {
                    _logger.LogInformation(ex, ex.Message);
                    HttpContext.Session.SetString(SessionKey.NotificationMessage.Error, $"{ExceptionExtensions.GetDescription(ex)} {ErrorMessage.ErrorMessagePostfix.ErrorDescription}");
                }
                else
                {
                    _logger.LogError(ex, ex.Message);
                    HttpContext.Session.SetString(SessionKey.NotificationMessage.Error, ErrorMessage.ErrorMessageDefault.ErrorDescription);
                }
            }

            return Redirect("~/Admin/List");
        }
    }
}
