using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BeamAndGo.Core.Common;
using BeamAndGo.Core.Data;
using BeamAndGo.Member.Admin;
using BeamAndGo.Member.Admin.Web.Filters;
using BeamAndGo.Member.Admin.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace BeamAndGo.Member.Admin.Web.Pages.Admin
{
    [LoginRequired]
    [ValidateAntiForgeryToken]
    public class AdminListModel : PageModel
    {
        private readonly ApplicationOptions _applicationOptions;
        private readonly ILogger<AdminListModel> _logger;
        private readonly IMemberAdminService _MemberAdminService;


        public IEnumerable<MemberAdmin> MemberAdmins { get; private set; }
        public string MinExpiryDate { get; set; }


        [BindProperty]
        public DateTimeOffset? ExpiryDate { get; set; }
        [BindProperty(SupportsGet = true)]
        public int CurrentPage { get; set; } = 1;
        [BindProperty(SupportsGet = true)]
        public int PagesCount { get; set; } = 1;
        [BindProperty]
        public MemberAdminAccessLevel AccessLevel { get; set; }
        [BindProperty]
        public string AdminMemberSid { get; set; }
        [BindProperty(SupportsGet = true)]
        public string CurrentSearch { get; set; } = string.Empty;
        [BindProperty]
        public RecordStatus AdminRecordStatus { get; set; }


        public AdminListModel(ILogger<AdminListModel> logger,
                              IMemberAdminService MemberAdminService,
                              IOptions<ApplicationOptions> applicationOptions)
        {
            _logger = logger;
            _MemberAdminService = MemberAdminService;
            _applicationOptions = applicationOptions.Value;
        }

        public async Task<IActionResult> OnGetAsync(string search = "", int start = 0)
        {
            try
            {
                var sid = HttpContext.Session.GetString(SessionKey.MemberSid);
                var admin = await _MemberAdminService.GetByMemberSid(sid);

                if (admin.AccessLevel == MemberAdminAccessLevel.SuperAdmin)
                {
                    int count = 0;
                    start = start > 1 ? start : 1;
                    var startPage = start > 0 ? ((start - 1) * _applicationOptions.PageSize) : 0;
                    MinExpiryDate = DateTime.Now.ToString("yyyy-MM-dd");

                    if (!string.IsNullOrEmpty(search))
                    {
                        var iAsyncMemberAdmin = _MemberAdminService.Search(search, startPage);
                        MemberAdmins = iAsyncMemberAdmin.ToEnumerable();
                        count = MemberAdmins.Count();
                    }
                    else
                    {
                        MemberAdmins = await _MemberAdminService.ListAll(startPage);
                        count = await _MemberAdminService.Count();
                    }

                    CurrentPage = start;
                    CurrentSearch = search;
                    PagesCount = ((count - 1) / _applicationOptions.PageSize) + 1;
                }
                else
                {
                    string currentPageForError = Request.Path.ToString().Replace("/", " ");
                    return RedirectToPage("/Error", new { error = ErrorMessage.AccessDenied.Error, error_description = ErrorMessage.AccessDenied.ErrorDescription, error_page = currentPageForError });
                }
            }
            catch (Exception ex)
            {
                if (ex is ArgumentException || ex is InvalidOperationException)
                {
                    _logger.LogInformation(ex, ex.Message);
                    return RedirectToPage("/Error", new { error = ExceptionExtensions.GetCode(ex), error_description = ExceptionExtensions.GetDescription(ex) });
                }
                else
                {
                    throw;
                }
            }

            return Page();
        }

        public async Task<IActionResult> OnPostRemoveAdmin()
        {
            try
            {
                await _MemberAdminService.Delete(AdminMemberSid);
                HttpContext.Session.SetString(SessionKey.NotificationMessage.Sucess, "Successfully removed admin");
            }
            catch (ArgumentException ex)
            {
                _logger.LogInformation(ex, ex.Message);
                HttpContext.Session.SetString(SessionKey.NotificationMessage.Error, $"{ExceptionExtensions.GetDescription(ex)}");
            }
            catch (Exception ex)
            {
                if (ex is InvalidOperationException)
                {
                    _logger.LogInformation(ex, ex.Message);
                    HttpContext.Session.SetString(SessionKey.NotificationMessage.Error, $"{ExceptionExtensions.GetDescription(ex)} {ErrorMessage.ErrorMessagePostfix.ErrorDescription}");
                }
                else
                {
                    _logger.LogError(ex, ex.Message);
                    HttpContext.Session.SetString(SessionKey.NotificationMessage.Error, ErrorMessage.ErrorMessageDefault.ErrorDescription);
                }
            }

            return Redirect("~/Admin/List");
        }

        public async Task<IActionResult> OnPostUpdateAdmin()
        {
            try
            {
                var MemberAdmin = await _MemberAdminService.Update(AdminMemberSid, AccessLevel, ExpiryDate);

                if (AdminRecordStatus != MemberAdmin.RecordStatus)
                {
                    if (AdminRecordStatus == RecordStatus.Active)
                    {
                        var memberEnable = await _MemberAdminService.Enable(AdminMemberSid);
                    }
                    else
                    {
                        var memberDisable = await _MemberAdminService.Disable(AdminMemberSid);
                    }
                }

                HttpContext.Session.SetString(SessionKey.NotificationMessage.Sucess, "Successfully updated admin");
            }
            catch (ArgumentException ex)
            {
                _logger.LogInformation(ex, ex.Message);
                HttpContext.Session.SetString(SessionKey.NotificationMessage.Error, $"{ExceptionExtensions.GetDescription(ex)}");
            }
            catch (Exception ex)
            {
                if (ex is InvalidOperationException)
                {
                    _logger.LogInformation(ex, ex.Message);
                    HttpContext.Session.SetString(SessionKey.NotificationMessage.Error, $"{ExceptionExtensions.GetDescription(ex)} {ErrorMessage.ErrorMessagePostfix.ErrorDescription}");
                }
                else
                {
                    _logger.LogError(ex, ex.Message);
                    HttpContext.Session.SetString(SessionKey.NotificationMessage.Error, ErrorMessage.ErrorMessageDefault.ErrorDescription);
                }
            }

            return Redirect("~/Admin/List");
        }
    }
}
