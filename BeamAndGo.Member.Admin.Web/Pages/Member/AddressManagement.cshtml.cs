using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BeamAndGo.Core.Common;
using BeamAndGo.Member;
using BeamAndGo.Member.Admin.Web.Filters;
using BeamAndGo.Member.Admin.Web.Models;
using BeamAndGo.Member.Admin.Web.Models.FormData;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace BeamAndGo.Member.Admin.Web.Pages.Member
{
    [LoginRequired]
    public class AddressManagementModel : PageModel
    {
        private readonly ApplicationOptions _applicationOptions;
        private readonly ILogger<AddressManagementModel> _logger;
        private readonly IMemberAddressService _memberAddressService;
        private readonly IMemberService _memberService;


        public bool KYCEnabled { get; set; }
        public bool IsXHR { get { return Request.Headers["X-Requested-With"] == "XMLHttpRequest"; } }
        public IAsyncEnumerable<MemberAddress> MemberAddresses { get; set; }
        public BeamAndGo.Member.Member Member { get; set; }
        public string MemberCountryCode { get; set; }


        [BindProperty]
        public AddressFormData AddressFormData { get; set; }
        [BindProperty]
        public string MemberAddressSid { get; set; }
        [BindProperty]
        public string SelectedMemberSid { get; set; }
        
     
        public AddressManagementModel(ILogger<AddressManagementModel> logger, 
                                      IMemberService memberService, 
                                      IMemberAddressService memberAddressService,  
                                      IOptions<ApplicationOptions> applicationOptions)
        {
            _logger = logger;
            _memberService = memberService;
            _memberAddressService = memberAddressService;
            _applicationOptions = applicationOptions.Value;
       }

        public async Task<IActionResult> OnGetAsync(string memberSid)
        {
            try
            {
                KYCEnabled = _applicationOptions.KYCEnabled;
                Member = (await _memberService.GetBySid(memberSid));
                MemberCountryCode = Member?.CountryCode;

                MemberAddresses = _memberAddressService.ListByMemberSid(Member.Sid);
            }
            catch (Exception ex)
            {
                if (ex is ArgumentException || ex is InvalidOperationException)
                {
                    _logger.LogInformation(ex, ex.Message);
                    return RedirectToPage("/Error", new { error = ExceptionExtensions.GetCode(ex), error_description = ExceptionExtensions.GetDescription(ex) });
                }
                else
                {
                    throw;
                }
            }

            return Page();
        }

        public async Task<IActionResult> OnPostResendAddressAsync()
        {
            try
            {
                //Temporary not in use
                await _memberAddressService.Verify(MemberAddressSid, string.Empty);

                HttpContext.Session.SetString(SessionKey.NotificationMessage.Sucess, "Successfully resend a verification to your email");
            }
            catch (ArgumentException ex)
            {
                _logger.LogInformation(ex, ex.Message);
                HttpContext.Session.SetString(SessionKey.NotificationMessage.Error, $"{ExceptionExtensions.GetDescription(ex)}");
            }
            catch (Exception ex)
            {
                if (ex is InvalidOperationException)
                {
                    _logger.LogInformation(ex, ex.Message);
                    HttpContext.Session.SetString(SessionKey.NotificationMessage.Error, $"{ExceptionExtensions.GetDescription(ex)} {ErrorMessage.ErrorMessagePostfix.ErrorDescription}");
                }
                else
                {
                    _logger.LogError(ex, "Failed to resend address {@MemberAddressSid}", MemberAddressSid);
                    HttpContext.Session.SetString(SessionKey.NotificationMessage.Error, ErrorMessage.ErrorMessageDefault.ErrorDescription);
                }
            }

            return Redirect($"~/Member/Edit?memberSid={SelectedMemberSid}");
        }

        public async Task<IActionResult> OnPostMakePrimaryAddressAsync(string addressSid)
        {
            try
            {
                await _memberAddressService.SetDefault(addressSid);
                return new OkObjectResult("Successfully updated your primary address");
            }
            catch (ArgumentException ex)
            {
                _logger.LogInformation(ex, ex.Message);
                return BadRequest($"{ExceptionExtensions.GetDescription(ex)}");
            }
            catch (Exception ex)
            {
                if (ex is InvalidOperationException)
                {
                    _logger.LogInformation(ex, ex.Message);
                    return BadRequest($"{ExceptionExtensions.GetDescription(ex)} {ErrorMessage.ErrorMessagePostfix.ErrorDescription}");
                }
                else
                {
                    _logger.LogError(ex, "Failed to make primary {@addressSid}", addressSid);
                    return BadRequest(ErrorMessage.ErrorMessageDefault.ErrorDescription);
                }
            }
        }

        public async Task<IActionResult> OnPostRemoveAddressAsync()
        {
            try
            {
                await _memberAddressService.Delete(MemberAddressSid);
                return new OkObjectResult("Successfully removed your address");
            }
            catch (ArgumentException ex)
            {
                _logger.LogInformation(ex, ex.Message);
                return BadRequest($"{ExceptionExtensions.GetDescription(ex)}");
            }
            catch (Exception ex)
            {
                if (ex is InvalidOperationException)
                {
                    _logger.LogInformation(ex, ex.Message);
                    return BadRequest($"{ExceptionExtensions.GetDescription(ex)} {ErrorMessage.ErrorMessagePostfix.ErrorDescription}");
                }
                else
                {
                    _logger.LogError(ex, "Failed to remove address {@MemberAddressSid}", MemberAddressSid);
                    return BadRequest(ErrorMessage.ErrorMessageDefault.ErrorDescription);
                }
            }
        }

        public async Task<IActionResult> OnPostAddAddressAsync()
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var sid = SelectedMemberSid;
                    await _memberAddressService.Create(
                        sid,
                        AddressFormData.Name,
                        AddressFormData.Street,
                        AddressFormData.PostalCode,
                        AddressFormData.Country
                    );
                    return new OkObjectResult("Successfully added your address");
                }
                else
                {
                    return BadRequest("Please enter a valid address");
                }
            }
            catch (ArgumentException ex)
            {
                _logger.LogInformation(ex, ex.Message);
                return BadRequest($"{ExceptionExtensions.GetDescription(ex)}");
            }
            catch (Exception ex)
            {
                if (ex is InvalidOperationException)
                {
                    _logger.LogInformation(ex, ex.Message);
                    return BadRequest($"{ExceptionExtensions.GetDescription(ex)} {ErrorMessage.ErrorMessagePostfix.ErrorDescription}");
                }
                else
                {
                    _logger.LogError(ex, "Failed to add address");
                    return BadRequest(ErrorMessage.ErrorMessageDefault.ErrorDescription);
                }
            }
        }
    }
}
