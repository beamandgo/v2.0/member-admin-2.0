using System;
using System.Threading.Tasks;
using BeamAndGo.Core.Common;
using BeamAndGo.Member.Signup;
using BeamAndGo.Member.Admin.Web.Filters;
using BeamAndGo.Member.Admin.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.Linq;

namespace BeamAndGo.Member.Admin.Web.Pages
{
    [LoginRequired]
    [ValidateAntiForgeryToken]
    public class CreateMemberModel : PageModel
    {
        private readonly ApplicationOptions _applicationOptions;
        private readonly ILogger<CreateMemberModel> _logger;
        private readonly IMemberSignupService _memberSignupService;


        public string MaxBirthdate { get; set; }
        public string MinBirthdate { get; set; }
        public string StaticAssetsUrl { get; set; }


        [BindProperty]
        public SignupData FormData { get; set; }
        [BindProperty]
        public string Birthdate { get; set; }


        public CreateMemberModel(ILogger<CreateMemberModel> logger,
                                 IMemberSignupService memberSignupService,
                                 IOptions<ApplicationOptions> applicationOptions)
        {
            _applicationOptions = applicationOptions.Value;
            _logger = logger;
            _memberSignupService = memberSignupService;
            FormData = new SignupData();
        }

        public IActionResult OnGet()
        {
            try
            {
                MinBirthdate = DateTime.Now.AddYears(-100).ToString("yyyy-MM-dd");
                MaxBirthdate = DateTime.Now.AddYears(-1).ToString("yyyy-MM-dd");
                StaticAssetsUrl = _applicationOptions.StaticAssetsUrl;
            }
            catch (Exception ex)
            {
                if (ex is ArgumentException || ex is InvalidOperationException)
                {
                    _logger.LogInformation(ex, ex.Message);
                    return RedirectToPage("/Error", new { error = ExceptionExtensions.GetCode(ex), error_description = ExceptionExtensions.GetDescription(ex) });
                }
                else
                {
                    throw;
                }
            }

            return Page();
        }

        public async Task<IActionResult> OnPostCreateAsync()
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var birthDate = Convert.ToDateTime(Birthdate);

                    if (birthDate.Year > 1)
                    {
                        FormData.BirthDay = birthDate.Day;
                        FormData.BirthMonth = birthDate.Month;
                        FormData.BirthYear = birthDate.Year;
                    }

                    var createdMember = await _memberSignupService.Signup(FormData);

                    return new OkObjectResult("Successfully created new member");
                }
                catch (ArgumentException ex)
                {
                    _logger.LogInformation(ex, ex.Message);
                    return BadRequest($"{ExceptionExtensions.GetDescription(ex)}");
                }
                catch (Exception ex)
                {
                    if (ex is InvalidOperationException)
                    {
                        _logger.LogInformation(ex, ex.Message);
                        return BadRequest($"{ExceptionExtensions.GetDescription(ex)} {ErrorMessage.ErrorMessagePostfix.ErrorDescription}");
                    }
                    else
                    {
                        _logger.LogError(ex, ex.Message);
                        return BadRequest(ErrorMessage.ErrorMessageDefault.ErrorDescription);
                    }
                }
            }
            else
            {
                var message = string.Join("\n", ModelState.Values
                                    .SelectMany(v => v.Errors)
                                    .Select(e => e.ErrorMessage));

                return BadRequest(message);
            }
        }
    }
}
