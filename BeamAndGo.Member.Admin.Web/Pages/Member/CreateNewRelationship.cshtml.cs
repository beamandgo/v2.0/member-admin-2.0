using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BeamAndGo.Core.Common;
using BeamAndGo.Member;
using BeamAndGo.Member.Admin.Web.Filters;
using BeamAndGo.Member.Admin.Web.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace BeamAndGo.Member.Admin.Web.Pages.Member
{
    [LoginRequired]
    public class CreateNewRelationshipModel : PageModel
    {
        private readonly ApplicationOptions _applicationOptions;
        private readonly ILogger<CreateNewRelationshipModel> _logger;
        private readonly IMemberRelationshipService _memberRelationshipService;
        private readonly IMemberRelationshipTypeService _memberRelationshipTypeService;
        private readonly IMemberService _memberService;


        public IEnumerable<MemberRelationshipType> RelationshipTypes { get; private set; }
        public bool IsXHR { get { return Request.Headers["X-Requested-With"] == "XMLHttpRequest"; } }
        public string MemberCountryCode { get; set; }
        public string MaxBirthdate { get; set; }
        public string MinBirthdate { get; set; }
        public string StaticAssetsUrl { get; set; }


        [BindProperty]
        public CreateMemberRelationshipData FormData { get; set; }
        [BindProperty]
        public string RelationshipBirthdate { get; set; }
        [BindProperty]
        public string RelationshipType { get; set; }
        [BindProperty]
        public string SelectedMemberSid { get; set; }


        public CreateNewRelationshipModel(ILogger<CreateNewRelationshipModel> logger,
                                          IMemberService memberService,
                                          IMemberRelationshipService memberRelationshipService,
                                          IMemberRelationshipTypeService memberRelationshipTypeService,
                                          IOptions<ApplicationOptions> applicationOptions)
        {
            _logger = logger;
            _memberService = memberService;
            _memberRelationshipService = memberRelationshipService;
            _memberRelationshipTypeService = memberRelationshipTypeService;
            _applicationOptions = applicationOptions.Value;
            FormData = new CreateMemberRelationshipData();
        }

        public async Task<IActionResult> OnGetAsync(string memberSid)
        {
            var sid = memberSid;

            try
            {
                SelectedMemberSid = memberSid;

                MinBirthdate = DateTime.Now.AddYears(-100).ToString("yyyy-MM-dd");
                MaxBirthdate = DateTime.Now.AddYears(-1).ToString("yyyy-MM-dd");
                StaticAssetsUrl = _applicationOptions.StaticAssetsUrl;

                RelationshipTypes = await _memberRelationshipTypeService.ListAllActiveSortedByName();

                var member = await _memberService.GetBySid(sid);
                MemberCountryCode = member?.CountryCode;
            }
            catch (Exception ex)
            {
                if (ex is ArgumentException || ex is InvalidOperationException)
                {
                    _logger.LogInformation(ex, ex.Message);
                    return RedirectToPage("/Error", new { error = ExceptionExtensions.GetCode(ex), error_description = ExceptionExtensions.GetDescription(ex) });
                }
                else
                {
                    throw;
                }
            }

            return Page();
        }

        public async Task<IActionResult> OnPostCreateAsync()
        {
            if (ModelState.IsValid)
            {
                try
                {
                    FormData.MemberSid = SelectedMemberSid;

                    var birthDate = Convert.ToDateTime(RelationshipBirthdate);
                    if (birthDate.Year > 1)
                    {
                        FormData.BirthDay = birthDate.Day;
                        FormData.BirthMonth = birthDate.Month;
                        FormData.BirthYear = birthDate.Year;
                    }

                    if (RelationshipType != null)
                    {
                        FormData.RelationshipTypeId = Convert.ToInt32(RelationshipType.Split("|")[0]);
                    }

                    var relationship = await _memberRelationshipService.Create(FormData);

                    return new OkObjectResult("Successfully added new relationship");
                }
                catch (ArgumentException ex)
                {
                    _logger.LogInformation(ex, ex.Message);
                    return BadRequest($"{ExceptionExtensions.GetDescription(ex)}");
                }
                catch (Exception ex)
                {
                    if (ex is InvalidOperationException)
                    {
                        _logger.LogInformation(ex, ex.Message);
                        return BadRequest($"{ExceptionExtensions.GetDescription(ex)} {ErrorMessage.ErrorMessagePostfix.ErrorDescription}");
                    }
                    else
                    {
                        _logger.LogError(ex, ex.Message);
                        return BadRequest(ErrorMessage.ErrorMessageDefault.ErrorDescription);
                    }
                }
            }
            else
            {
                var message = string.Join("\n", ModelState.Values
                                    .SelectMany(v => v.Errors)
                                    .Select(e => e.ErrorMessage));

                return BadRequest(message);
            }
        }
    }
}
