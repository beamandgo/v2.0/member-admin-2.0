using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BeamAndGo.Core.Common;
using BeamAndGo.Core.Country;
using BeamAndGo.Core.Data;
using BeamAndGo.Member;
using BeamAndGo.Member.Admin;
using BeamAndGo.Member.Admin.Web.Filters;
using BeamAndGo.Member.Admin.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace BeamAndGo.Member.Admin.Web.Pages.Member
{
    [LoginRequired]
    [ValidateAntiForgeryToken]
    public class MemberEditModel : PageModel
    {
        private readonly ICountryService _countryService;
        private readonly ApplicationOptions _applicationOptions;
        private readonly ILogger<MemberEditModel> _logger;
        private readonly IMemberAdminService _MemberAdminService;
        private readonly IMemberService _memberService;
        private readonly IMemberRelationshipService _memberRelationshipService;
        private readonly IMemberRelationshipTypeService _memberRelationshipTypeService;

        public bool HasRelationships { get; set; }
        public bool KYCEnabled { get; set; } = false;
        public bool KYCVerified { get; set; } = false;
        public bool MemberEnabled { get; set; }
        public string CountryName { get; set; }

        public IAsyncEnumerable<MemberRelationship> Relationships { get; private set; }
        public IEnumerable<MemberRelationshipType> RelationshipTypes { get; private set; }
        public MemberAddress Address { get; set; }
        public MemberEmail Email { get; set; }
        public MemberPhone Phone { get; set; }
        public string MaxBirthdate { get; set; }
        public string MinBirthdate { get; set; }


        [BindProperty]
        public string Birthdate { get; set; }
        [BindProperty]
        public string MemberSid { get; set; }
        [BindProperty]
        public string RelationshipBirthdate { get; set; }
        [BindProperty]
        public string RelationshipType { get; set; }
        [BindProperty]
        public string SelectedRelationshipSid { get; set; }
        [BindProperty]
        public UpdateMemberData FormData { get; set; }
        [BindProperty]
        public UpdateMemberRelationshipData RelationshipFormData { get; set; }


        public MemberEditModel(ILogger<MemberEditModel> logger,
                                ICountryService countryService,
                                IMemberAdminService MemberAdminService,
                                IMemberService memberService,
                                IMemberRelationshipService memberRelationshipService,
                                IMemberRelationshipTypeService memberRelationshipTypeService,
                                IOptions<ApplicationOptions> applicationOptions)
        {
            _logger = logger;
            _countryService = countryService;
            _memberService = memberService;
            _MemberAdminService = MemberAdminService;
            _memberRelationshipService = memberRelationshipService;
            _memberRelationshipTypeService = memberRelationshipTypeService;

            Address = new MemberAddress();
            Phone = new MemberPhone();
            Email = new MemberEmail();
            FormData = new UpdateMemberData();
            RelationshipFormData = new UpdateMemberRelationshipData();
            _applicationOptions = applicationOptions.Value;
        }

        public async Task<IActionResult> OnGetAsync(string memberSid)
        {
            try
            {
                if (string.IsNullOrEmpty(memberSid))
                    return RedirectToPage("./Member/List");

                var usersid = HttpContext.Session.GetString(SessionKey.MemberSid);
                var admin = await _MemberAdminService.GetByMemberSid(usersid);

                if (admin.AccessLevel == MemberAdminAccessLevel.FullAccess || admin.AccessLevel == MemberAdminAccessLevel.SuperAdmin)
                {
                    MemberSid = memberSid;
                    var member = await _memberService.GetBySid(memberSid, true);
                    var country = await _countryService.GetByAlpha2Code(member.CountryCode);
                    FormData.FirstName = member.FirstName;
                    FormData.LastName = member.LastName;
                    FormData.MiddleName = string.IsNullOrEmpty(member.MiddleName) ? "" : member.MiddleName;
                    FormData.DisplayName = string.IsNullOrEmpty(member.DisplayName) ? "" : member.DisplayName;
                    FormData.CountryCode = member.CountryCode;
                    FormData.Gender = member.Gender;

                    CountryName = country.Name;

                    if (member.BirthDay == null || member.BirthMonth == null || member.BirthYear == null)
                    {
                        Birthdate = string.Empty;
                    }
                    else
                    {
                        Birthdate = new DateTime((int)member.BirthYear, (int)member.BirthMonth, (int)member.BirthDay).ToString("yyyy-MM-dd");
                    }

                    if (member.Phones != null)
                    {
                        Phone = member.Phones.Select(x => x).FirstOrDefault(x => x.MemberId == member.Id && x.IsDefault == true);
                    }

                    if (member.Emails != null)
                    {
                        Email = member.Emails.Select(x => x).FirstOrDefault(x => x.MemberId == member.Id && x.IsDefault == true);
                    }

                    if (member.Addresses != null)
                    {
                        Address = member.Addresses.Select(x => x).FirstOrDefault(x => x.MemberId == member.Id && x.IsDefault == true);
                    }

                    MinBirthdate = DateTime.Now.AddYears(-100).ToString("yyyy-MM-dd");
                    MaxBirthdate = DateTime.Now.AddYears(-1).ToString("yyyy-MM-dd");
                    KYCEnabled = _applicationOptions.KYCEnabled;

                    if (Phone != null && Email != null && Address != null)
                    {
                        KYCVerified = (Phone.IsVerified == true && Email.IsVerified == true && Address.IsVerified == true);
                    }

                    MemberEnabled = (member.RecordStatus == RecordStatus.Active);

                    Relationships = _memberRelationshipService.ListByMemberSid(member.Sid);
                    RelationshipTypes = await _memberRelationshipTypeService.ListAllActiveSortedByName();
                    HasRelationships = await Relationships.AnyAsync().ConfigureAwait(false);
                }
                else
                {
                    string currentPageForError = Request.Path.ToString().Replace("/", " ");
                    return RedirectToPage("/Error", new { error = ErrorMessage.AccessDenied.Error, error_description = ErrorMessage.AccessDenied.ErrorDescription, error_page = currentPageForError });
                }
            }
            catch (Exception ex)
            {
                if (ex is ArgumentException || ex is InvalidOperationException)
                {
                    _logger.LogInformation(ex, ex.Message);
                    return RedirectToPage("/Error", new { error = ExceptionExtensions.GetCode(ex), error_description = ExceptionExtensions.GetDescription(ex) });
                }
                else
                {
                    throw;
                }
            }

            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var birthDate = Convert.ToDateTime(Birthdate);

                    if (birthDate.Year > 1)
                    {
                        FormData.BirthDay = birthDate.Day;
                        FormData.BirthMonth = birthDate.Month;
                        FormData.BirthYear = birthDate.Year;
                    }

                    await _memberService.Update(MemberSid, FormData);

                    HttpContext.Session.SetString(SessionKey.NotificationMessage.Sucess, "Successfully updated member information");
                }
                catch (ArgumentException ex)
                {
                    _logger.LogInformation(ex, ex.Message);
                    HttpContext.Session.SetString(SessionKey.NotificationMessage.Error, $"{ExceptionExtensions.GetDescription(ex)}");
                }
                catch (Exception ex)
                {
                    if (ex is InvalidOperationException)
                    {
                        _logger.LogInformation(ex, ex.Message);
                        HttpContext.Session.SetString(SessionKey.NotificationMessage.Error, $"{ExceptionExtensions.GetDescription(ex)} {ErrorMessage.ErrorMessagePostfix.ErrorDescription}");
                    }
                    else
                    {
                        _logger.LogError(ex, ex.Message);
                        HttpContext.Session.SetString(SessionKey.NotificationMessage.Error, ErrorMessage.ErrorMessageDefault.ErrorDescription);
                    }
                }
            }

            return Redirect($"~/Member/Edit?memberSid={MemberSid}");
        }

        public async Task<IActionResult> OnPostEnableMemberAsync()
        {
            try
            {

                await _memberService.Enable(MemberSid);

                HttpContext.Session.SetString(SessionKey.NotificationMessage.Sucess, "Successfully enabled member");

            }
            catch (ArgumentException ex)
            {
                _logger.LogInformation(ex, ex.Message);
                HttpContext.Session.SetString(SessionKey.NotificationMessage.Error, $"{ExceptionExtensions.GetDescription(ex)}");
            }
            catch (Exception ex)
            {
                if (ex is InvalidOperationException)
                {
                    _logger.LogInformation(ex, ex.Message);
                    HttpContext.Session.SetString(SessionKey.NotificationMessage.Error, $"{ExceptionExtensions.GetDescription(ex)} {ErrorMessage.ErrorMessagePostfix.ErrorDescription}");
                }
                else
                {
                    _logger.LogError(ex, ex.Message);
                    HttpContext.Session.SetString(SessionKey.NotificationMessage.Error, ErrorMessage.ErrorMessageDefault.ErrorDescription);
                }
            }

            return Redirect($"~/Member/Edit?memberSid={MemberSid}");

        }

        public async Task<IActionResult> OnPostDisableMemberAsync()
        {
            try
            {
                await _memberService.Disable(MemberSid);

                HttpContext.Session.SetString(SessionKey.NotificationMessage.Sucess, "Successfully disabled member");
            }
            catch (ArgumentException ex)
            {
                _logger.LogInformation(ex, ex.Message);
                HttpContext.Session.SetString(SessionKey.NotificationMessage.Error, $"{ExceptionExtensions.GetDescription(ex)}");
            }
            catch (Exception ex)
            {
                if (ex is InvalidOperationException)
                {
                    _logger.LogInformation(ex, ex.Message);
                    HttpContext.Session.SetString(SessionKey.NotificationMessage.Error, $"{ExceptionExtensions.GetDescription(ex)} {ErrorMessage.ErrorMessagePostfix.ErrorDescription}");
                }
                else
                {
                    _logger.LogError(ex, ex.Message);
                    HttpContext.Session.SetString(SessionKey.NotificationMessage.Error, ErrorMessage.ErrorMessageDefault.ErrorDescription);
                }
            }

            return Redirect($"~/Member/Edit?memberSid={MemberSid}");

        }

        public async Task<IActionResult> OnPostUpdateRelationshipAsync()
        {
            try
            {
                var birthDate = Convert.ToDateTime(RelationshipBirthdate);

                if (birthDate.Year > 1)
                {
                    RelationshipFormData.BirthDay = birthDate.Day;
                    RelationshipFormData.BirthMonth = birthDate.Month;
                    RelationshipFormData.BirthYear = birthDate.Year;
                }

                if (RelationshipType != null)
                {
                    RelationshipFormData.RelationshipTypeId = Convert.ToInt32(RelationshipType.Split("|")[0]);
                }

                var relationship = await _memberRelationshipService.Update(SelectedRelationshipSid, RelationshipFormData);

                return new OkObjectResult("Successfully updated your relationship");
            }
            catch (ArgumentException ex)
            {
                _logger.LogInformation(ex, ex.Message);
                return BadRequest($"{ExceptionExtensions.GetDescription(ex)}");
            }
            catch (Exception ex)
            {
                if (ex is InvalidOperationException)
                {
                    _logger.LogInformation(ex, ex.Message);
                    return BadRequest($"{ExceptionExtensions.GetDescription(ex)} {ErrorMessage.ErrorMessagePostfix.ErrorDescription}");
                }
                else
                {
                    _logger.LogError(ex, ex.Message);
                    return BadRequest(ErrorMessage.ErrorMessageDefault.ErrorDescription);
                }
            }
        }

        public async Task<IActionResult> OnPostRemoveRelationship()
        {
            try
            {
                await _memberRelationshipService.Delete(SelectedRelationshipSid);

                HttpContext.Session.SetString(SessionKey.NotificationMessage.Sucess, "Successfully removed relationship");
            }
            catch (ArgumentException ex)
            {
                _logger.LogInformation(ex, ex.Message);
                HttpContext.Session.SetString(SessionKey.NotificationMessage.Error, $"{ExceptionExtensions.GetDescription(ex)}");
            }
            catch (Exception ex)
            {
                if (ex is InvalidOperationException)
                {
                    _logger.LogInformation(ex, ex.Message);
                    HttpContext.Session.SetString(SessionKey.NotificationMessage.Error, $"{ExceptionExtensions.GetDescription(ex)} {ErrorMessage.ErrorMessagePostfix.ErrorDescription}");
                }
                else
                {
                    _logger.LogError(ex, ex.Message);
                    HttpContext.Session.SetString(SessionKey.NotificationMessage.Error, ErrorMessage.ErrorMessageDefault.ErrorDescription);
                }
            }

            return Redirect($"~/Member/Edit?memberSid={MemberSid}");
        }

    }
}
