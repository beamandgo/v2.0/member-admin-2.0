using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using BeamAndGo.Core.Common;
using BeamAndGo.Member;
using BeamAndGo.Member.Verification;
using BeamAndGo.Member.Admin.Web.Filters;
using BeamAndGo.Member.Admin.Web.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace BeamAndGo.Member.Admin.Web.Pages.Member
{
    [LoginRequired]
    public class EmailManagementModel : PageModel
    {
        private readonly ApplicationOptions _options;
        private readonly ILogger<EmailManagementModel> _logger;
        private readonly IMemberEmailService _memberEmailService;
        private readonly IMemberEmailVerificationService _memberEmailVerificationService;


        public bool IsXHR { get { return Request.Headers["X-Requested-With"] == "XMLHttpRequest"; } }
        public IAsyncEnumerable<MemberEmail> MemberEmails { get; set; }


        [BindProperty]
        [DataType(DataType.EmailAddress)]
        public string NewEmail { get; set; }
        [BindProperty]
        public string SelectedEmailSid { get; set; }
        [BindProperty]
        public string SelectedMemberSid { get; set; }

        
        public EmailManagementModel(ILogger<EmailManagementModel> logger,  
                                    IMemberEmailService memberEmailService, 
                                    IMemberEmailVerificationService memberEmailVerificationService,
                                    IOptions<ApplicationOptions> options)
        {
            _logger = logger;
            _memberEmailService = memberEmailService;
            _memberEmailVerificationService = memberEmailVerificationService;
            _options = options.Value;
        }

        public IActionResult OnGet(string memberSid)
        {
            try
            {
                SelectedMemberSid = memberSid;

                MemberEmails = _memberEmailService.ListByMemberSid(memberSid);
            }
            catch (Exception ex)
            {
                if (ex is ArgumentException || ex is InvalidOperationException)
                {
                    _logger.LogInformation(ex, ex.Message);
                    return RedirectToPage("/Error", new { error = ExceptionExtensions.GetCode(ex), error_description = ExceptionExtensions.GetDescription(ex) });
                }
                else
                {
                    throw;
                }
            }

            return Page();
        }

        public async Task<IActionResult> OnPostAddAsync()
        {
            try
            {
                if (string.IsNullOrEmpty(NewEmail))
                {
                    return BadRequest("Please enter your e-mail address");
                }
                else
                {
                    var email = await _memberEmailService.Create(SelectedMemberSid, NewEmail);
                    return new OkObjectResult("Successfully added " + email.Email);
                }
            }
            catch (ArgumentException ex)
            {
                _logger.LogInformation(ex, ex.Message);
                return BadRequest($"{ExceptionExtensions.GetDescription(ex)}");
            }
            catch (Exception ex)
            {
                if (ex is InvalidOperationException)
                {
                    _logger.LogInformation(ex, ex.Message);
                    return BadRequest($"{ExceptionExtensions.GetDescription(ex)} {ErrorMessage.ErrorMessagePostfix.ErrorDescription}");
                }
                else
                {
                    _logger.LogError(ex, "Failed to add e-mail address {@NewEmail}", NewEmail);
                    return BadRequest(ErrorMessage.ErrorMessageDefault.ErrorDescription);
                }
            }
        }

        public async Task<IActionResult> OnPostRemoveAsync()
        {
            try
            {
                await _memberEmailService.Delete(SelectedEmailSid);
                return new OkObjectResult("Successfully removed email");
            }
            catch (ArgumentException ex)
            {
                _logger.LogInformation(ex, ex.Message);
                return BadRequest($"{ExceptionExtensions.GetDescription(ex)}");
            }
            catch (Exception ex)
            {
                if (ex is InvalidOperationException)
                {
                    _logger.LogInformation(ex, ex.Message);
                    return BadRequest($"{ExceptionExtensions.GetDescription(ex)} {ErrorMessage.ErrorMessagePostfix.ErrorDescription}");
                }
                else
                {
                    _logger.LogError(ex, "Failed to remove e-mail address for sid {@SelectedEmailSid}", SelectedEmailSid);
                    return BadRequest(ErrorMessage.ErrorMessageDefault.ErrorDescription);
                }
            }
        }

        public async Task<IActionResult> OnPostResendVerificationAsync(string emailSid)
        {
            try
            {
                var verificationLink = _options.MemberWebUrl + _options.MailVerificationUrl + "?code={VerificationCode}";
                await _memberEmailVerificationService.Send(emailSid, verificationLink);

                var email = await _memberEmailService.GetBySid(emailSid);
                return new OkObjectResult(email.Email);
            }
            catch (ArgumentException ex)
            {
                _logger.LogInformation(ex, ex.Message);
                return BadRequest($"{ExceptionExtensions.GetDescription(ex)}");
            }
            catch (Exception ex)
            {
                if (ex is InvalidOperationException)
                {
                    _logger.LogInformation(ex, ex.Message);
                    return BadRequest($"{ExceptionExtensions.GetDescription(ex)} {ErrorMessage.ErrorMessagePostfix.ErrorDescription}");
                }
                else
                {
                    _logger.LogError(ex, "Failed to generate verification for email sid {@emailSid}", emailSid);
                    return BadRequest(ErrorMessage.ErrorMessageDefault.ErrorDescription);
                }
            }
        }

        public async Task<IActionResult> OnPostMakePrimaryAsync(string emailSid)
        {
            try
            {
                await _memberEmailService.SetDefault(emailSid);

                return new OkObjectResult("Successfully updated default email");
            }
            catch (ArgumentException ex)
            {
                _logger.LogInformation(ex, ex.Message);
                return BadRequest($"{ExceptionExtensions.GetDescription(ex)}");
            }
            catch (Exception ex)
            {
                if (ex is InvalidOperationException)
                {
                    _logger.LogInformation(ex, ex.Message);
                    return BadRequest($"{ExceptionExtensions.GetDescription(ex)} {ErrorMessage.ErrorMessagePostfix.ErrorDescription}");
                }
                else
                {
                    _logger.LogError(ex, "Failed to make e-mail address primary for email sid {@emailSid}", emailSid);
                    return BadRequest(ErrorMessage.ErrorMessageDefault.ErrorDescription);
                }
            }
        }

    }
}
