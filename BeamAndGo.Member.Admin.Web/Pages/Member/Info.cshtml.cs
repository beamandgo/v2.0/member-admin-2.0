using System;
using System.Threading.Tasks;
using BeamAndGo.Core.Common;
using BeamAndGo.Member;
using BeamAndGo.Member.Admin;
using BeamAndGo.Member.Admin.Web.Filters;
using BeamAndGo.Member.Admin.Web.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;

namespace BeamAndGo.Member.Admin.Web.Pages.Member
{
    [LoginRequired]
    public class MemberInfoModel : PageModel
    {
        private readonly ILogger<MemberInfoModel> _logger;
        private readonly IMemberAdminService _MemberAdminService;
        private readonly IMemberService _memberService;


        public bool IsReadOnly = true;
        private bool IsMasked = false;
        public MemberViewModel member;


        public MemberInfoModel(ILogger<MemberInfoModel> logger,
                               IMemberAdminService MemberAdminService,
                               IMemberService memberService)
        {
            _logger = logger;
            _memberService = memberService;
            _MemberAdminService = MemberAdminService;
        }

        public async Task<IActionResult> OnGetAsync(string sid)
        {
            try
            {
                var userSid = HttpContext.Session.GetString(SessionKey.MemberSid);

                var admin = await _MemberAdminService.GetByMemberSid(userSid);

                var m = await _memberService.GetBySid(sid);

                if (admin.AccessLevel == MemberAdminAccessLevel.FullAccess)
                {
                    member = new MemberViewModel(m, IsMasked);

                    IsReadOnly = false;
                }
                else if (admin.AccessLevel == MemberAdminAccessLevel.ReadOnlyUnmasked)
                {
                    member = new MemberViewModel(m, IsMasked);
                }
                else if (admin.AccessLevel == MemberAdminAccessLevel.ReadOnlyMasked)
                {
                    IsMasked = true;

                    member = new MemberViewModel(m, IsMasked);
                }
            }
            catch (Exception ex)
            {
                if (ex is ArgumentException || ex is InvalidOperationException)
                {
                    _logger.LogInformation(ex, ex.Message);
                    return RedirectToPage("/Error", new { error = ExceptionExtensions.GetCode(ex), error_description = ExceptionExtensions.GetDescription(ex) });
                }
                else
                {
                    throw;
                }
            }

            return Page();
        }

    }
}
