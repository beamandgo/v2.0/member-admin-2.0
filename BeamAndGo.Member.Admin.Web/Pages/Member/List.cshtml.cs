using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BeamAndGo.Core.Common;
using BeamAndGo.Member;
using BeamAndGo.Member.Admin;
using BeamAndGo.Member.Admin.Web.Filters;
using BeamAndGo.Member.Admin.Web.Models;
using BeamAndGo.Member.Admin.Web.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace BeamAndGo.Member.Admin.Web.Pages.Member
{
    [LoginRequired]
    [ValidateAntiForgeryToken]
    public class MemberListModel : PageModel
    {
        private readonly ApplicationOptions _applicationOptions;
        private readonly ILogger<MemberListModel> _logger;
        private readonly IMemberService _memberService;
        private readonly IMemberAdminService _MemberAdminService;
        

        [BindProperty(SupportsGet = true)]
        public int CurrentPage { get; set; } = 1;

        [BindProperty(SupportsGet = true)]
        public string CurrentSearch { get; set; } = string.Empty;

        [BindProperty(SupportsGet = true)]
        public int PagesCount { get; set; } = 1;

        public IEnumerable<MemberViewModel> memberList = Enumerable.Empty<MemberViewModel>();
        public IEnumerable<BeamAndGo.Member.Member> Members { get; set; }
        public bool IsReadOnly = true;
        private bool IsMasked = false;

        public MemberListModel(ILogger<MemberListModel> logger, 
                               IMemberAdminService MemberAdminService,
                               IMemberService memberService,
                               IOptions<ApplicationOptions> applicationOptions)
        {
            _logger = logger;
            _memberService = memberService;
            _MemberAdminService = MemberAdminService;
            _applicationOptions = applicationOptions.Value;
        }

        public async Task<IActionResult> OnGetAsync(string search = "", int start = 0)
        {
            try
            {
                int count = 0;
                var sid = HttpContext.Session.GetString(SessionKey.MemberSid);
                var admin = await _MemberAdminService.GetByMemberSid(sid);

                start = start > 1 ? start : 1;

                var startPage = start > 0 ? ((start - 1) * _applicationOptions.PageSize) : 0;

                if (!string.IsNullOrEmpty(search))
                {
                    var iAsyncMember = _memberService.Search(search, startPage);
                    Members = iAsyncMember.ToEnumerable(); 

                    count = Members.Count();
                }
                else
                {
                    Members = await _memberService.ListAll(startPage);

                    count = await _memberService.Count();
                }

                CurrentPage = start;
                CurrentSearch = search;
                PagesCount = ((count - 1) / _applicationOptions.PageSize) + 1;

                if (admin.AccessLevel == MemberAdminAccessLevel.FullAccess || admin.AccessLevel == MemberAdminAccessLevel.SuperAdmin)
                {
                    foreach (var member in Members)
                    {
                        memberList = memberList.Append(new MemberViewModel(member, IsMasked));
                    }

                    IsReadOnly = false;
                }
                else if (admin.AccessLevel == MemberAdminAccessLevel.ReadOnlyUnmasked)
                {
                    foreach (var member in Members)
                    {
                        memberList = memberList.Append(new MemberViewModel(member, IsMasked));
                    }
                }
                else if (admin.AccessLevel == MemberAdminAccessLevel.ReadOnlyMasked)
                {
                    IsMasked = true;

                    foreach (var member in Members)
                    {
                        memberList = memberList.Append(new MemberViewModel(member, IsMasked));
                    }
                }
                else
                {
                    string currentPageForError = Request.Path.ToString().Replace("/", " ");
                    return RedirectToPage("/Error", new { error = ErrorMessage.AccessDenied.Error, error_description = ErrorMessage.AccessDenied.ErrorDescription, error_page = currentPageForError });
                }
            }
            catch (Exception ex)
            {
                if (ex is ArgumentException || ex is InvalidOperationException)
                {
                    _logger.LogInformation(ex, ex.Message);
                    return RedirectToPage("/Error", new { error = ExceptionExtensions.GetCode(ex), error_description = ExceptionExtensions.GetDescription(ex) });
                }
                else
                {
                    throw;
                }
            }

            return Page();
        }
    }
}
