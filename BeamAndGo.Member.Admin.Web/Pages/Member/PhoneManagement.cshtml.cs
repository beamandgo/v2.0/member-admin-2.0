using BeamAndGo.Core.Common;
using BeamAndGo.Core.Data;
using BeamAndGo.Member;
using BeamAndGo.Member.Verification;
using BeamAndGo.Member.Admin.Web.Filters;
using BeamAndGo.Member.Admin.Web.Models;
using BeamAndGo.Member.Admin.Web.Models.FormData;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeamAndGo.Member.Admin.Web.Pages.Member
{
    [LoginRequired]
    public class PhoneManagementModel : PageModel
    {
        private readonly ApplicationOptions _applicationOptions;
        private readonly ILogger<PhoneManagementModel> _logger;
        private readonly IMemberService _memberService;
        private readonly IMemberPhoneService _memberPhoneService;
        private readonly IMemberPhoneVerificationService _memberPhoneVerificationService;


        public bool IsXHR { get { return Request.Headers["X-Requested-With"] == "XMLHttpRequest"; } }
        public IAsyncEnumerable<MemberPhone> MemberPhones { get; set; }
        public string MemberCountryCode { get; set; }
        public string StaticAssetsUrl { get; set; }


        [BindProperty]
        public PhoneFormData FormData { get; set; }
        [BindProperty(SupportsGet = true)]
        public string SelectedPhoneSid { get; set; }
        [BindProperty]
        public string SelectedMemberSid { get; set; }
        [BindProperty]
        public string VerificationPrefix { get; set; }
        [BindProperty]
        public string VerificationCode { get; set; }
        

        public PhoneManagementModel(ILogger<PhoneManagementModel> logger, 
                                    IOptions<ApplicationOptions> applicationOptions, 
                                    IMemberService memberService,
                                    IMemberPhoneService memberPhoneService, 
                                    IMemberPhoneVerificationService memberPhoneVerificationService)
        {
            _logger = logger;
            _memberService = memberService;
            _memberPhoneService = memberPhoneService;
            _memberPhoneVerificationService = memberPhoneVerificationService;
            _applicationOptions = applicationOptions.Value;
            FormData = new PhoneFormData();
        }

        public async Task<IActionResult> OnGetAsync(string memberSid)
        {
            try
            {
                StaticAssetsUrl = _applicationOptions.StaticAssetsUrl;
                var member = await _memberService.GetBySid(memberSid);
                SelectedMemberSid = member.Sid;
                MemberCountryCode = member?.CountryCode;

                MemberPhones = _memberPhoneService.ListByMemberSid(member.Sid);
            }
            catch (Exception ex)
            {
                if (ex is ArgumentException || ex is InvalidOperationException)
                {
                    _logger.LogInformation(ex, ex.Message);
                    return RedirectToPage("/Error", new { error = ExceptionExtensions.GetCode(ex), error_description = ExceptionExtensions.GetDescription(ex) });
                }
                else
                {
                    throw;
                }
            }

            return Page();
        }

        public async Task<IActionResult> OnPostAddAsync()
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var memberSid = SelectedMemberSid;
                    FormData.MobileNumber = FormData.MobileNumber.SanitizePhone();

                    var phone = await _memberPhoneService.Create(memberSid, FormData.CallingCode, FormData.MobileNumber);
                    //otp
                    return new OkObjectResult($"Successfully added +{phone.CallingCode} {phone.PhoneNumber}");
                }
                catch (ArgumentException ex)
                {
                    _logger.LogInformation(ex, ex.Message);
                    return BadRequest($"{ExceptionExtensions.GetDescription(ex)}");
                }
                catch (Exception ex)
                {
                    if (ex is InvalidOperationException)
                    {
                        _logger.LogInformation(ex, ex.Message);
                        return BadRequest($"{ExceptionExtensions.GetDescription(ex)} {ErrorMessage.ErrorMessagePostfix.ErrorDescription}");
                    }
                    else
                    {
                        _logger.LogError(ex, ex.Message);
                        return BadRequest(ErrorMessage.ErrorMessageDefault.ErrorDescription);
                    }
                }
            }
            else
            {
                return BadRequest("Please enter your phone number");
            }
        }

        public async Task<IActionResult> OnPostRemoveAsync()
        {
            try
            {
                await _memberPhoneService.Delete(SelectedPhoneSid);
                return new OkObjectResult($"Successfully removed phone number");
            }
            catch (ArgumentException ex)
            {
                _logger.LogInformation(ex, ex.Message);
                return BadRequest($"{ExceptionExtensions.GetDescription(ex)}");
            }
            catch (Exception ex)
            {
                if (ex is InvalidOperationException)
                {
                    _logger.LogInformation(ex, ex.Message);
                    return BadRequest($"{ExceptionExtensions.GetDescription(ex)} {ErrorMessage.ErrorMessagePostfix.ErrorDescription}");
                }
                else
                {
                    _logger.LogError(ex, ex.Message);
                    return BadRequest(ErrorMessage.ErrorMessageDefault.ErrorDescription);
                }
            }
        }

        public async Task<IActionResult> OnPostResendVerificationAsync(string phoneSid)
        {
            try
            {
                if (!string.IsNullOrEmpty(SelectedPhoneSid)) phoneSid = SelectedPhoneSid;

                var prefix = await _memberPhoneVerificationService.SendVerification(phoneSid);
                var phone = await _memberPhoneService.GetBySid(phoneSid);

                return new OkObjectResult($"+{phone.CallingCode} {phone.PhoneNumber},{prefix}");
            }
            catch (ArgumentException ex)
            {
                _logger.LogInformation(ex, ex.Message);
                return BadRequest($"{ExceptionExtensions.GetDescription(ex)}");
            }
            catch (Exception ex)
            {
                if (ex is InvalidOperationException)
                {
                    _logger.LogInformation(ex, ex.Message);
                    return BadRequest($"{ExceptionExtensions.GetDescription(ex)} {ErrorMessage.ErrorMessagePostfix.ErrorDescription}");
                }
                else
                {
                    _logger.LogError(ex, ex.Message);
                    return BadRequest(ErrorMessage.ErrorMessageDefault.ErrorDescription);
                }
            }
        }

        public async Task<IActionResult> OnPostMakePrimaryAsync(string phoneSid)
        {
            try
            {
                var sid = SelectedMemberSid;

                // Set selected phone as primary
                await _memberPhoneService.SetDefault(phoneSid);

                var phone = await _memberPhoneService.GetBySid(phoneSid);

                return new OkObjectResult($"Successfully set primary to +{phone.CallingCode} {phone.PhoneNumber}");
            }
            catch (ArgumentException ex)
            {
                _logger.LogInformation(ex, ex.Message);
                return BadRequest($"{ExceptionExtensions.GetDescription(ex)}");
            }
            catch (Exception ex)
            {
                if (ex is InvalidOperationException)
                {
                    _logger.LogInformation(ex, ex.Message);
                    return BadRequest($"{ExceptionExtensions.GetDescription(ex)} {ErrorMessage.ErrorMessagePostfix.ErrorDescription}");
                }
                else
                {
                    _logger.LogError(ex, ex.Message);
                    return BadRequest(ErrorMessage.ErrorMessageDefault.ErrorDescription);
                }
            }
        }

        public async Task<IActionResult> OnPostVerifyAsync()
        {
            try
            {
                var verificationResult = await _memberPhoneVerificationService.Verify(SelectedPhoneSid, VerificationPrefix, VerificationCode);
                var phone = await _memberPhoneService.GetBySid(SelectedPhoneSid);

                return new OkObjectResult($"Successfully verified +{phone.CallingCode} {phone.PhoneNumber}");
            }
            catch (ArgumentException ex)
            {
                _logger.LogInformation(ex, ex.Message);
                return BadRequest($"{ExceptionExtensions.GetDescription(ex)}");
            }
            catch (Exception ex)
            {
                if (ex is InvalidOperationException)
                {
                    _logger.LogInformation(ex, ex.Message);
                    return BadRequest($"{ExceptionExtensions.GetDescription(ex)} {ErrorMessage.ErrorMessagePostfix.ErrorDescription}");
                }
                else
                {
                    _logger.LogError(ex, ex.Message);
                    return BadRequest(ErrorMessage.ErrorMessageDefault.ErrorDescription);
                }
            }
        }
    }
}
