using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BeamAndGo.Core.Common;
using BeamAndGo.Member;
using BeamAndGo.Member.Admin.Web.Filters;
using BeamAndGo.Member.Admin.Web.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;

namespace BeamAndGo.Member.Admin.Web.Pages.Member
{
    [LoginRequired]
    public class PreferenceModel : PageModel
    {
        private readonly ILogger<PreferenceModel> _logger;
        private readonly IMemberPreferenceService _memberPreferenceService;
        private readonly IMemberPreferenceTypeService _memberPreferenceTypeService;


        public bool IsXHR { get { return Request.Headers["X-Requested-With"] == "XMLHttpRequest"; } }
        public IAsyncEnumerable<MemberPreference> MemberPreferences { get; set; }


        [BindProperty]
        public string CurrentSelectedValue { get; set; }
        [BindProperty]
        public string SelectedMemberSid { get; set; }
        [BindProperty]
        public string SelectedPreferenceTypeId { get; set; }


        public PreferenceModel(ILogger<PreferenceModel> logger,
                                            IMemberPreferenceService memberPreferenceService,
                                            IMemberPreferenceTypeService memberPreferenceTypeService)
        {
            _logger = logger;
            _memberPreferenceService = memberPreferenceService;
            _memberPreferenceTypeService = memberPreferenceTypeService;
        }

        public IActionResult OnGet(string memberSid)
        {
            var sid = memberSid;

            try
            {
                SelectedMemberSid = sid;
                MemberPreferences = _memberPreferenceService.ListWithDefaultByMemberSid(memberSid);
            }
            catch (Exception ex)
            {
                if (ex is ArgumentException || ex is InvalidOperationException)
                {
                    _logger.LogInformation(ex, ex.Message);
                    return RedirectToPage("/Error", new { error = ExceptionExtensions.GetCode(ex), error_description = ExceptionExtensions.GetDescription(ex) });
                }
                else
                {
                    throw;
                }
            }

            return Page();
        }

        public async Task<IActionResult> OnPostUpdatePreferenceAsync()
        {
            var sid = SelectedMemberSid;

            try
            {
                if (CurrentSelectedValue.ToLower() == "true")
                {
                    await _memberPreferenceService.CreateOrUpdate(sid, Convert.ToInt32(SelectedPreferenceTypeId), "true");
                }
                else
                {
                    await _memberPreferenceService.CreateOrUpdate(sid, Convert.ToInt32(SelectedPreferenceTypeId), "false");
                }

                return new OkObjectResult("Successfully updated Preference");
            }
            catch (ArgumentException ex)
            {
                _logger.LogInformation(ex, ex.Message);
                return BadRequest($"{ExceptionExtensions.GetDescription(ex)}");
            }
            catch (Exception ex)
            {
                if (ex is InvalidOperationException)
                {
                    _logger.LogInformation(ex, ex.Message);
                    return BadRequest($"{ExceptionExtensions.GetDescription(ex)} {ErrorMessage.ErrorMessagePostfix.ErrorDescription}");
                }
                else
                {
                    _logger.LogError(ex, $"Failed to update Communication Preference");
                    return BadRequest(ErrorMessage.ErrorMessageDefault.ErrorDescription);
                }
            }
        }
    }
}
