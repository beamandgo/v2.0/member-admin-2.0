using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BeamAndGo.Core.Common;
using BeamAndGo.Core.Data;
using BeamAndGo.Member;
using BeamAndGo.Member.Admin;
using BeamAndGo.Member.Admin.Web.Filters;
using BeamAndGo.Member.Admin.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;

namespace BeamAndGo.Member.Admin.Web.Pages.PreferenceType
{
    [ValidateAntiForgeryToken]
    [LoginRequired]
    public class PreferenceTypeListModel : PageModel
    {
        private readonly ILogger<PreferenceTypeListModel> _logger;
        private readonly IMemberAdminService _MemberAdminService;
        private readonly IMemberPreferenceTypeService _memberPreferenceTypeService;

        [BindProperty]
        public int PreferenceTypeID { get; set; }

        [BindProperty]
        public string PreferenceTypeName { get; set; }

        [BindProperty]
        public string PreferenceTypeDescription { get; set; }

        [BindProperty]
        public string PreferenceTypeDefaultValue { get; set; }

        [BindProperty]
        public RecordStatus PreferenceTypeStatus { get; set; }
        [BindProperty]
        public RecordStatus CurrentPreferenceTypeStatus { get; set; }
        public IEnumerable<MemberPreferenceType> MemberPreferenceTypes { get; private set; }

        public PreferenceTypeListModel(ILogger<PreferenceTypeListModel> logger, IMemberAdminService MemberAdminService, IMemberPreferenceTypeService memberPreferenceTypeService)
        {
            _logger = logger;
            _MemberAdminService = MemberAdminService;
            _memberPreferenceTypeService = memberPreferenceTypeService;
        }

        public async Task<IActionResult> OnGetAsync()
        {
            try
            {
                var sid = HttpContext.Session.GetString(SessionKey.MemberSid);
                var admin = await _MemberAdminService.GetByMemberSid(sid);

                if (admin.AccessLevel == MemberAdminAccessLevel.SuperAdmin)
                {
                    MemberPreferenceTypes = await _memberPreferenceTypeService.ListAll();
                }
                else
                {
                    string currentPageForError = Request.Path.ToString().Replace("/", " ");
                    return RedirectToPage("/Error", new { error = ErrorMessage.AccessDenied.Error, error_description = ErrorMessage.AccessDenied.ErrorDescription, error_page = currentPageForError });
                }
            }
            catch (Exception ex)
            {
                if (ex is ArgumentException || ex is InvalidOperationException)
                {
                    _logger.LogInformation(ex, ex.Message);
                    return RedirectToPage("/Error", new { error = ExceptionExtensions.GetCode(ex), error_description = ExceptionExtensions.GetDescription(ex) });
                }
                else
                {
                    throw;
                }
            }

            return Page();
        }

        public async Task<IActionResult> OnPostCreatePreferenceType()
        {
            try
            {
                var adminMemberSid = HttpContext.Session.GetString(SessionKey.MemberSid);

                var MemberAdmin = await _memberPreferenceTypeService.Create(PreferenceTypeName, PreferenceTypeDescription, PreferenceTypeDefaultValue);

                HttpContext.Session.SetString(SessionKey.NotificationMessage.Sucess, "Successfully created preference type");
            }
            catch (ArgumentException ex)
            {
                _logger.LogInformation(ex, ex.Message);
                HttpContext.Session.SetString(SessionKey.NotificationMessage.Error, $"{ExceptionExtensions.GetDescription(ex)}");
            }
            catch (Exception ex)
            {
                if (ex is InvalidOperationException)
                {
                    _logger.LogInformation(ex, ex.Message);
                    HttpContext.Session.SetString(SessionKey.NotificationMessage.Error, $"{ExceptionExtensions.GetDescription(ex)} {ErrorMessage.ErrorMessagePostfix.ErrorDescription}");
                }
                else
                {
                    _logger.LogError(ex, ex.Message);
                    HttpContext.Session.SetString(SessionKey.NotificationMessage.Error, ErrorMessage.ErrorMessageDefault.ErrorDescription);
                }
            }

            return Redirect("~/PreferenceType/List");
        }

        public async Task<IActionResult> OnPostRemovePreferenceType()
        {
            try
            {
                var adminMemberSid = HttpContext.Session.GetString(SessionKey.MemberSid);

                await _memberPreferenceTypeService.Delete(PreferenceTypeID);
                HttpContext.Session.SetString(SessionKey.NotificationMessage.Sucess, "Successfully removed preference type");
            }
            catch (ArgumentException ex)
            {
                _logger.LogInformation(ex, ex.Message);
                HttpContext.Session.SetString(SessionKey.NotificationMessage.Error, $"{ExceptionExtensions.GetDescription(ex)}");
            }
            catch (Exception ex)
            {
                if (ex is InvalidOperationException)
                {
                    _logger.LogInformation(ex, ex.Message);
                    HttpContext.Session.SetString(SessionKey.NotificationMessage.Error, $"{ExceptionExtensions.GetDescription(ex)} {ErrorMessage.ErrorMessagePostfix.ErrorDescription}");
                }
                else
                {
                    _logger.LogError(ex, ex.Message);
                    HttpContext.Session.SetString(SessionKey.NotificationMessage.Error, ErrorMessage.ErrorMessageDefault.ErrorDescription);
                }
            }
          
            return Redirect("~/PreferenceType/List");
        }

        public async Task<IActionResult> OnPostUpdatePreferenceType()
        {
            try
            {
                var adminMemberSid = HttpContext.Session.GetString(SessionKey.MemberSid);

                var MemberAdmin = await _memberPreferenceTypeService.Update(PreferenceTypeID, PreferenceTypeName, PreferenceTypeDescription, PreferenceTypeDefaultValue);

                if (CurrentPreferenceTypeStatus != PreferenceTypeStatus)
                {
                    if (PreferenceTypeStatus == RecordStatus.Active)
                    {
                        await _memberPreferenceTypeService.Enable(PreferenceTypeID);
                    }
                    else
                    {
                        await _memberPreferenceTypeService.Disable(PreferenceTypeID);
                    }
                }

                HttpContext.Session.SetString(SessionKey.NotificationMessage.Sucess, "Successfully updated preference type");
            }
            catch (ArgumentException ex)
            {
                _logger.LogInformation(ex, ex.Message);
                HttpContext.Session.SetString(SessionKey.NotificationMessage.Error, $"{ExceptionExtensions.GetDescription(ex)}");
            }
            catch (Exception ex)
            {
                if (ex is InvalidOperationException)
                {
                    _logger.LogInformation(ex, ex.Message);
                    HttpContext.Session.SetString(SessionKey.NotificationMessage.Error, $"{ExceptionExtensions.GetDescription(ex)} {ErrorMessage.ErrorMessagePostfix.ErrorDescription}");
                }
                else
                {
                    _logger.LogError(ex, ex.Message);
                    HttpContext.Session.SetString(SessionKey.NotificationMessage.Error, ErrorMessage.ErrorMessageDefault.ErrorDescription);
                }
            }
           
            return Redirect("~/PreferenceType/List");
        }

    }
}
