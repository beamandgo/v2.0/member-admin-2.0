using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BeamAndGo.Core.Common;
using BeamAndGo.Core.Data;
using BeamAndGo.Member;
using BeamAndGo.Member.Admin;
using BeamAndGo.Member.Admin.Web.Filters;
using BeamAndGo.Member.Admin.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;

namespace BeamAndGo.Member.Admin.Web.Pages.RelationshipType
{
    [ValidateAntiForgeryToken]
    [LoginRequired]
    public class RelationshipTypeListModel : PageModel
    {
        private readonly ILogger<RelationshipTypeListModel> _logger;
        private readonly IMemberAdminService _MemberAdminService;
        private readonly IMemberRelationshipTypeService _memberRelationshipTypeService;

        [BindProperty]
        public char? RelationshipTypeGender { get; set; }

        [BindProperty]
        public int RelationshipTypeId { get; set; }

        [BindProperty]
        public string RelationshipTypeName { get; set; }

        [BindProperty]
        public RecordStatus RelationshipTypeStatus { get; set; }
        [BindProperty]
        public RecordStatus CurrentRelationshipTypeStatus { get; set; }



        public IEnumerable<MemberRelationshipType> MemberRelationshipTypes { get; private set; }

        public RelationshipTypeListModel(ILogger<RelationshipTypeListModel> logger, IMemberAdminService MemberAdminService, IMemberRelationshipTypeService memberRelationshipTypeService)
        {
            _logger = logger;
            _MemberAdminService = MemberAdminService;
            _memberRelationshipTypeService = memberRelationshipTypeService;
        }

        public async Task<IActionResult> OnGetAsync()
        {
            try
            {
                var sid = HttpContext.Session.GetString(SessionKey.MemberSid);
                var admin = await _MemberAdminService.GetByMemberSid(sid);

                if (admin.AccessLevel == MemberAdminAccessLevel.SuperAdmin)
                {
                    MemberRelationshipTypes = await _memberRelationshipTypeService.ListAll();
                }
                else
                {
                    string currentPageForError = Request.Path.ToString().Replace("/", " ");
                    return RedirectToPage("/Error", new { error = ErrorMessage.AccessDenied.Error, error_description = ErrorMessage.AccessDenied.ErrorDescription, error_page = currentPageForError });
                }

            }
            catch (Exception ex)
            {
                if (ex is ArgumentException || ex is InvalidOperationException)
                {
                    _logger.LogInformation(ex, ex.Message);
                    return RedirectToPage("/Error", new { error = ExceptionExtensions.GetCode(ex), error_description = ExceptionExtensions.GetDescription(ex) });
                }
                else
                {
                    throw;
                }
            }

            return Page();
        }

        public async Task<IActionResult> OnPostCreateRelationshipType()
        {
            try
            {
                var adminMemberSid = HttpContext.Session.GetString(SessionKey.MemberSid);

                var MemberAdmin = await _memberRelationshipTypeService.Create(RelationshipTypeName, RelationshipTypeGender);

                HttpContext.Session.SetString(SessionKey.NotificationMessage.Sucess, "Successfully created relationship type");
            }
            catch (ArgumentException ex)
            {
                _logger.LogInformation(ex, ex.Message);
                HttpContext.Session.SetString(SessionKey.NotificationMessage.Error, $"{ExceptionExtensions.GetDescription(ex)}");
            }
            catch (Exception ex)
            {
                if (ex is InvalidOperationException)
                {
                    _logger.LogInformation(ex, ex.Message);
                    HttpContext.Session.SetString(SessionKey.NotificationMessage.Error, $"{ExceptionExtensions.GetDescription(ex)} {ErrorMessage.ErrorMessagePostfix.ErrorDescription}");
                }
                else
                {
                    _logger.LogError(ex, ex.Message);
                    HttpContext.Session.SetString(SessionKey.NotificationMessage.Error, ErrorMessage.ErrorMessageDefault.ErrorDescription);
                }
            }

            return Redirect("~/RelationshipType/List");
        }

        public async Task<IActionResult> OnPostRemoveRelationshipType()
        {
            try
            {
                var adminMemberSid = HttpContext.Session.GetString(SessionKey.MemberSid);

                await _memberRelationshipTypeService.Delete(RelationshipTypeId);
                HttpContext.Session.SetString(SessionKey.NotificationMessage.Sucess, "Successfully removed relationship type");
            }
            catch (ArgumentException ex)
            {
                _logger.LogInformation(ex, ex.Message);
                HttpContext.Session.SetString(SessionKey.NotificationMessage.Error, $"{ExceptionExtensions.GetDescription(ex)}");
            }
            catch (Exception ex)
            {
                if (ex is InvalidOperationException)
                {
                    _logger.LogInformation(ex, ex.Message);
                    HttpContext.Session.SetString(SessionKey.NotificationMessage.Error, $"{ExceptionExtensions.GetDescription(ex)} {ErrorMessage.ErrorMessagePostfix.ErrorDescription}");
                }
                else
                {
                    _logger.LogError(ex, ex.Message);
                    HttpContext.Session.SetString(SessionKey.NotificationMessage.Error, ErrorMessage.ErrorMessageDefault.ErrorDescription);
                }
            }

            return Redirect("~/RelationshipType/List");
        }

        public async Task<IActionResult> OnPostUpdateRelationshipType()
        {
            try
            {
                var adminMemberSid = HttpContext.Session.GetString(SessionKey.MemberSid);

                var MemberAdmin = await _memberRelationshipTypeService.Update(RelationshipTypeId, RelationshipTypeName, RelationshipTypeGender);


                if (CurrentRelationshipTypeStatus != RelationshipTypeStatus)
                {
                    if (RelationshipTypeStatus == RecordStatus.Active)
                    {
                        await _memberRelationshipTypeService.Enable(RelationshipTypeId);
                    }
                    else
                    {
                        await _memberRelationshipTypeService.Disable(RelationshipTypeId);
                    }
                }

                HttpContext.Session.SetString(SessionKey.NotificationMessage.Sucess, "Successfully updated relationship type");
            }
            catch (ArgumentException ex)
            {
                _logger.LogInformation(ex, ex.Message);
                HttpContext.Session.SetString(SessionKey.NotificationMessage.Error, $"{ExceptionExtensions.GetDescription(ex)}");
            }
            catch (Exception ex)
            {
                if (ex is InvalidOperationException)
                {
                    _logger.LogInformation(ex, ex.Message);
                    HttpContext.Session.SetString(SessionKey.NotificationMessage.Error, $"{ExceptionExtensions.GetDescription(ex)} {ErrorMessage.ErrorMessagePostfix.ErrorDescription}");
                }
                else
                {
                    _logger.LogError(ex, ex.Message);
                    HttpContext.Session.SetString(SessionKey.NotificationMessage.Error, ErrorMessage.ErrorMessageDefault.ErrorDescription);
                }
            }

            return Redirect("~/RelationshipType/List");
        }
    }
}
