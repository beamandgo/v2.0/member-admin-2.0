﻿using System;

namespace BeamAndGo.Member.Admin.Web
{
    public static class SessionKey
    {
        public const string MemberSid = "MemberSid";
        public const string RedirectUrl = "RedirectUrl";

        /// <summary>
        /// Auth Token
        /// </summary>
        public static class Token
        {
            public const string AccessToken = "Token.AccessToken";
            public const string RefreshToken = "Token.RefreshToken";
            public const string IdToken = "Token.IdToken";
            public const string Expiry = "Token.Expiry";
            public const string Scope = "Token.Scope";
            public const string State = "Token.State";
        }

        public static class NotificationMessage
        {
            public const string Default = "NotificationMessage.Default";
            public const string Sucess = "NotificationMessage.Sucess";
            public const string Error = "NotificationMessage.Error";
        }
    }
}
