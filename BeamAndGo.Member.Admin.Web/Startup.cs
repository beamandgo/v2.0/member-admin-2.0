using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using StackExchange.Redis;
using BeamAndGo.Core.Common;
using BeamAndGo.Member.Data;
using BeamAndGo.Auth.SDK;
using BeamAndGo.Member.Admin.Web.Filters;
using Microsoft.OpenApi.Models;
using System.Collections.Generic;
using BeamAndGo.Member;
using Microsoft.AspNetCore.DataProtection;
using BeamAndGo.Member.Remitro.Proxy;
using BeamAndGo.Remitro.Contexts;
using BeamAndGo.Member.Messaging;
using BeamAndGo.Remitro.Messaging;
using BeamAndGo.Member.Admin;
using BeamAndGo.Member.Admin.Data;
using BeamAndGo.Member.Verification;
using BeamAndGo.Member.Invite;
using BeamAndGo.Member.Verification.Data;
using BeamAndGo.Member.Signup;
using BeamAndGo.Messaging.Client;
using Grpc.Net.Client;
using BeamAndGo.Core.Country;
using BeamAndGo.Core.Country.RestCountries;
using BeamAndGo.Core.Country.GeoIP2;
using BeamAndGo.Core.AspNetCore;

namespace BeamAndGo.Member.Admin.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Configure Kestrel Server
            services.Configure<KestrelServerOptions>(options => options.AddServerHeader = false);
            services.AddResponseCompression();
            services.Configure<ForwardedHeadersOptions>(options =>
            {
                options.ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto;
                options.KnownNetworks.Clear();
                options.KnownProxies.Clear();
            });

            // Use Redis Database as a cache
            services.AddStackExchangeRedisCache(options =>
            {
                options.Configuration = Configuration.GetConnectionString("Redis");
            });


            // Key storage providers in ASP.NET Core
            // https://docs.microsoft.com/en-us/aspnet/core/security/data-protection/implementation/key-storage-providers?view=aspnetcore-3.1&tabs=visual-studio
            services.AddDataProtection().PersistKeysToStackExchangeRedis(ConnectionMultiplexer.Connect(Configuration.GetConnectionString("Redis")));

            // Add Redis Database IConnectionMultiplexer to DI Container
            services.AddSingleton<IConnectionMultiplexer>(ConnectionMultiplexer.Connect(Configuration.GetConnectionString("Redis")));

            // Add Database Contexts
            services.AddDbContext<MemberDbContext>(options => options.UseMySql(Configuration.GetConnectionString("Member")));
            services.AddDbContext<MemberAdminDbContext>(options => options.UseMySql(Configuration.GetConnectionString("Member")));
            services.AddDbContextPool<DbContextRemitro>(options => options.EnableDetailedErrors().UseMySql(Configuration.GetConnectionString("Remitro")));

            // Configure and add services
            services.Configure<ApplicationOptions>(Configuration.GetSection("Application"));
            services.Configure<IOptions<MemberServiceOptions>>(Configuration.GetSection("Member"));
            services.Configure<MemberMessagingOptions>(Configuration.GetSection("MemberMessaging"));
            services.Configure<IOptions<MemberVerificationServiceOptions>>(Configuration.GetSection("MemberVerification"));

            var channel = GrpcChannel.ForAddress(Configuration.GetConnectionString("Grpc"));

            services.AddScoped<IMemberRepositoryManager, MemberDbRepositoryManager>();
            services.AddScoped<IMemberAdminRepositoryManager, MemberAdminDbRepositoryManager>();
            services.AddScoped<IMemberEmailVerificationTokenRepository, MemberEmailVerificationTokenRedisRepository>();

            services.AddScoped<IMemberEmailInviteFormatter, BeamAndGoMemberEmailInviteFormatter>();
            services.AddScoped<IMemberPhoneInviteFormatter, BeamAndGoMemberPhoneInviteFormatter>();
            services.AddScoped<IMemberEmailVerificationFormatter, DefaultEmailVerificationFormatter>();

            services.AddSingleton<IMailMessagingClient>(s => new MailMessagingClient(channel));
            services.AddSingleton<ITextMessagingClient>(s => new TextMessagingClient(channel));
            services.AddSingleton<IOneTimePinClient>(s => new OneTimePinClient(channel));

            services.AddScoped<IMemberMailMessagingProvider, GrpcMailMessagingProvider>();
            services.AddScoped<IMemberOtpProvider, GrpcOtpProvider>();
            services.AddScoped<IMemberTextMessagingProvider, GrpcTextMessagingProvider>();

            services.AddScoped<IMemberLogService, MemberLogService>();
            services.AddScoped<IMemberSearch, MemberDbSearch>();
            services.AddScoped<IMemberAdminLogService, MemberAdminLogService>();
            services.AddScoped<IMemberAdminSearch, MemberAdminDbSearch>();

            services.AddScoped<ICountryProvider, RestCountriesCountryProvider>();
            services.AddScoped<IIPAddressCountryLookupProvider, GeoIP2IPAddressCountryLookupProvider>();
            services.AddScoped<ICountryService, CountryService>();

            services.AddScoped<IMemberService, MemberRemitroProxyService>();
            services.AddScoped<IMemberAdminService, MemberAdminRemitroProxyService>();

            services.AddScoped<IMemberAddressService, MemberAddressRemitroProxyService>();
            services.AddScoped<IMemberEmailService, MemberEmailRemitroProxyService>();
            services.AddScoped<IMemberEmailInviteService, MemberEmailInviteRemitroProxyService>();
            services.AddScoped<IMemberEmailVerificationService, MemberEmailVerificationService>();
            services.AddScoped<IMemberPhoneService, MemberPhoneRemitroProxyService>();
            services.AddScoped<IMemberPhoneInviteService, MemberPhoneInviteRemitroProxyService>();
            services.AddScoped<IMemberPhoneVerificationService, MemberPhoneVerificationService>();
            services.AddScoped<IMemberPreferenceTypeService, MemberPreferenceTypeService>();
            services.AddScoped<IMemberPreferenceService, MemberPreferencesRemitroProxyService>();
            services.AddScoped<IMemberReferralService, MemberReferralRemitroProxyService>();
            services.AddScoped<IMemberRelationshipTypeService, MemberRelationshipTypeService>();
            services.AddScoped<IMemberRelationshipService, MemberRelationshipRemitroProxyService>();
            services.AddScoped<IMemberSignupService, MemberSignupRemitroProxyService>();

            services.Configure<AuthOptions>(Configuration.GetSection("Auth"));
            services.AddScoped<AuthClient>();

            services.AddSwaggerGen(swagger => {
                swagger.SwaggerDoc(name: "v2", new OpenApiInfo { Title = "BeamAndGoAPI", Version = "v2" });

                swagger.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Description = $"JWT Authorization header using the Bearer scheme." +
                                   "Enter 'Bearer' [space] and then your token in the text input below. " +
                                   "Example: 'Bearer 12345abcdef'",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer"
                });

                swagger.AddSecurityRequirement(new OpenApiSecurityRequirement()
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            },
                            Scheme = "oauth2",
                            Name = "Bearer",
                            In = ParameterLocation.Header,
                         },
                        new List<string>()
                    }
                });
            });

            // Add site-wide filter to provide configurations
            services.AddMvc(options => options.Filters.Add<LayoutPageFilter>());

            // Configure sessions
            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromDays(30); // We want a long timeout to keep the user logged in
                options.Cookie.Name = "_session"; // This just masks away any ASP.NET traces in our app, OK to hardcode
                options.Cookie.HttpOnly = true; // This is a MUST and should remain hardcoded so the cookie cannot be read with JS
                options.Cookie.IsEssential = true; // This bypasses GDPR requirements
            });

            // Configure CSRF
            services.AddAntiforgery(options =>
            {
                options.Cookie.Name = "_csrf"; // This just masks away any ASP.NET traces in our app, OK to hardcode
                options.FormFieldName = "_csrf"; // This just masks away any ASP.NET traces in our app, OK to hardcode
                options.HeaderName = "X-CSRF-TOKEN"; // This just masks away any ASP.NET traces in our app, OK to hardcode
                options.SuppressXFrameOptionsHeader = false;
            });

            // Force lowercase URLs
            services.AddRouting(options => options.LowercaseUrls = true);

            // Add Controllers and set the default JSON serlizer options
            services.AddControllers().AddJsonOptions(options => options.JsonSerializerOptions.DefaultOptions());

            // Add Razor Pages with Runtime Compilation (useful for debugging)
            services.AddRazorPages().AddRazorRuntimeCompilation();

            // Add Health Checks
            services
                .AddHealthChecks()
                .AddRedis(Configuration.GetConnectionString("Redis"), "Redis Database");
            //.AddDbContextCheck<CoreDbContext>("Core DbContext");
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint
            app.UseSwaggerUI(swagger =>
            {
                swagger.SwaggerEndpoint("/swagger/v2/swagger.json", "BeamAndGo API Version 1");
            });

            // Added to enable compression (if this does not work, we will take this out and use Nginx reverse proxy to do that)
            app.UseResponseCompression();

            app.UseForwardedHeaders();
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            // If you need to allow requests from different domains, uncomment the following line to allow Cross-Origin Requests (CORS)
            //app.UseCors();

            // Enable sessions
            app.UseSession();

            // Configure cookie policy (for any manually set cookies)
            app.UseCookiePolicy(new CookiePolicyOptions
            {
                Secure = Microsoft.AspNetCore.Http.CookieSecurePolicy.SameAsRequest,
                HttpOnly = Microsoft.AspNetCore.CookiePolicy.HttpOnlyPolicy.Always
            });

            // Enable localization
            app.UseRequestLocalization(app.ApplicationServices.GetService<IOptions<RequestLocalizationOptions>>().Value);

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                       name: "default",
                       pattern: "{controller=Member}/{action=List}");

                endpoints.MapControllers();
                endpoints.MapRazorPages();
            });

            // Configure Health Check
            app.UseHealthChecks("/health", HealthCheckHelper.DefaultHealthCheckOptions());
        }
    }
}
