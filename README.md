# BeamAndGo Member Administration for Platform 2.0

## Synopsis

Administration frontend for Member 2.0; also part of project SEGUIDA.

| Setting | Value |
| - | - |
| Target framework | .NET Core 3.1 |
| Namespace/Package | [BeamAndGo.Member.Admin.Web](BeamAndGo.Member.Admin.Web) |

## Documentation

Please refer to documentation at
[https://beamandgo.atlassian.net/wiki/spaces/OSS/pages/104464385/Project+SEGUIDA](https://beamandgo.atlassian.net/wiki/spaces/OSS/pages/104464385/Project+SEGUIDA).

## Contribute

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.
Please make sure to update tests as appropriate.

For more info, see [https://beamandgo.atlassian.net/wiki/spaces/OSS/pages/540409865/Contributing](https://beamandgo.atlassian.net/wiki/spaces/OSS/pages/540409865/Contributing).

## Maintainers

- Nan Zarchi Kyaw: nan.zk@beamandgo.com
- Justin Lee: justin.lee@beamandgo.com
- Jonathan E. Chua: jonathan.chua@beamandgo.com

## License

This project is licensed under the terms of the [GNU General Public License v3](https://www.gnu.org/licenses/gpl-3.0.en.html) open source license. Please refer to [https://beamandgo.atlassian.net/wiki/spaces/OSS/pages/538640400/GNU+GPL+v3](https://beamandgo.atlassian.net/wiki/spaces/OSS/pages/538640400/GNU+GPL+v3) for the full terms.
