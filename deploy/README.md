# Deployment Files and Scripts

## Instructions

This directory contains deployment files and scripts. In most circumstances,
the minimum is an [ENV.txt](ENV.txt) file for containerized apps.

If this directory contains only the [ENV.txt](ENV.txt) file, you may delete
this [README.md](README.md) file.

If you have custom files or scripts, you __should__ replace this with a custom
[README.md](README.md) file describing the purpose of each of the
file or script, and how to use it.

## ENV.txt

[ENV.txt](ENV.txt) is a template for the `.env` file. The `.env` file contains
environment variables that will be set when running apps in a Docker container.

Environment variables override values in `appsettings.json` during runtime.

Sensitive information such as passwords, encryption salts, private keys, or
auth tokens should _NEVER_ be checked in - always use mock values.

### Converting `appsettings.json` to `.env`

Sample `appsettings.json`:

	"Logging": {
	  "LogLevel": {
	    "Default": "Information"
	  }
	}

The above will translate to the following `.env` file:

	Logging__LogLevel__Default=Information

Take note of the following:
1. Double underscores `__` are used for nesting.
2. There are no quotes `""` placed around the values.

## License

Proprietary license &copy; BeamAndGo Pte Ltd.